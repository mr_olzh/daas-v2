package kz.clouddelivery.daas.holders;


import java.util.Locale;

public class ConstantsHolder {

    //Tags
    public static final String TAG_LOGIN = "login";
    public static final String TAG_PASSWORD = "password";
    public static final String TAG_LOGIN_AND_PASSWORD = "LoginAndPassword";
    //Applications
    public static final String COLLECTION_APPLICATIONS = "applications";
    public static final String COLLECTION_COMMENTS = "comments";
    public static final String COLLECTION_STATUSES = "statuses";
    //Subscribes
    public static final String SUBSCRIBE_APPLICATION = "applicationsMobileNew";
    public static final String SUBSCRIBE_STATUSES = "statusesMobileNew";
    public static final String SUBSCRIBE_COMMENTS = "commentsMobileNew";
    //Calls
    public static final String CALL_SET_ANDROID_REG_ID = "SetAndroidRegId";
    public static final String CALL_GET_COURIER_PROFILE = "GetCourierProfile";
    public static final String CALL_SET_COURIER_STATUS = "setCourierStatus";
    public static final String CALL_GET_COURIER_STATUSES = "getCourierStatuses";
    public static final String CALL_SEND_COURIER_POSITION = "SendCourierPosition";
    public static final String CALL_CHANGE_APP_STATUS_BY_COURIER = "changeAppStatusByCourier";
    public static final String CALL_SEND_COMMENT = "sendMessage";
    public static final String CALL_GOODS = "GetAppGoodsMobileNew";
    public static final String CALL_GET_APP_BY_ID_MOBILE = "GetAppByIdMobileNew";
    public static final String CALL_GET_IMAGE_URL_MOBILE = "GetImageUrlMobileNew";
    public static final String CALL_COURIER_START = "courierStart";
    public static final String CALL_LOG_OUT = "courierLogout";
    public static final String CALL_CHECK_STATUS_CONNECTION = "checkStatusConnection";
    //Meteor
    public static final String TAG_METEOR_RESULT_ERROR = "Meteor result error";
    public static final String TAG_METEOR_EXCEPTION = "Meteor exception";
    //Other
    public static final String EMPTY_STRING = "";
    public static final Locale LOCALE_RU = new Locale("ru", "RU");
    //Default
    //daas.relog.kz
    public static final String DEFAULT_IP = "daas.relog.kz";
    public static final String DEFAULT_FONT_PATH = "fonts/OpenSansRegular.ttf";
    static final String DEFAULT_PATH = "wss://" + DEFAULT_IP + "/websocket";

}
