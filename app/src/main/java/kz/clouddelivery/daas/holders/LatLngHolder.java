package kz.clouddelivery.daas.holders;

import com.google.android.gms.maps.model.LatLng;

public class LatLngHolder {

    public static boolean shouldSendPosition(LatLng position, LatLng position2) {
        double lat1 = position.latitude;
        double long1 = position.longitude;

        double lat2 = position2.latitude;
        double long2 = position2.longitude;

        double distance = LatLngHolder.distance(lat1, long1, lat2, long2, "K");
        double THRESHOLD_DISTANCE = 0.05;
        return distance >= THRESHOLD_DISTANCE;
    }

    private static double distance(double lat1, double lon1, double lat2, double lon2, String unit) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        if (unit == "K") {
            dist = dist * 1.609344;
        } else if (unit == "N") {
            dist = dist * 0.8684;
        }

        return (dist);
    }

    private static double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private static double rad2deg(double rad) {
        return (rad * 180 / Math.PI);
    }
}
