package kz.clouddelivery.daas.holders;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;


public class YandexNavigatorHolder {

    private static final String TAG_FROM_LATITUDE = "lat_from";
    private static final String TAG_FROM_LONGITUDE = "lon_from";
    private static final String TAG_TO_LATITUDE = "lat_to";
    private static final String TAG_TO_LONGITUDE = "lon_to";
    private static final String TAG_YANDEX_PACKAGE = "ru.yandex.yandexnavi";
    private static final String TAG_YANDEX_INTENT = "ru.yandex.yandexnavi.action.BUILD_ROUTE_ON_MAP";
    private static final String TAG_YANDEX_URI = "market://details?id=ru.yandex.yandexnavi";

    public YandexNavigatorHolder(Activity activity, LatLng from, LatLng to) {

        Intent intent = new Intent(TAG_YANDEX_INTENT);
        intent.setPackage(TAG_YANDEX_PACKAGE);

        PackageManager pm = activity.getPackageManager();
        List<ResolveInfo> infos = pm.queryIntentActivities(intent, 0);


        if (infos == null || infos.size() == 0) {
            intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(TAG_YANDEX_URI));
        } else {
            intent.putExtra(TAG_FROM_LATITUDE, from.latitude);
            intent.putExtra(TAG_FROM_LONGITUDE, from.longitude);
            intent.putExtra(TAG_TO_LATITUDE, to.latitude);
            intent.putExtra(TAG_TO_LONGITUDE, to.longitude);
        }

        activity.startActivity(intent);
    }
}
