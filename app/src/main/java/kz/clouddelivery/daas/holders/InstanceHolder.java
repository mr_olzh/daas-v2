package kz.clouddelivery.daas.holders;

import android.content.Context;
import android.support.annotation.Nullable;

import im.delight.android.ddp.MeteorSingleton;
import im.delight.android.ddp.db.memory.InMemoryDatabase;

public class InstanceHolder {

    private static MeteorSingleton mMeteor;

    @Nullable
    public static MeteorSingleton createMeteorInstance(Context context) {
        if (MeteorSingleton.hasInstance())
            MeteorSingleton.destroyInstance();
        mMeteor = MeteorSingleton.createInstance(context, ConstantsHolder.DEFAULT_PATH, new InMemoryDatabase());
        mMeteor.connect();
        return mMeteor;
    }

    public static void disconnectMeteor() {
        if (mMeteor != null) {
            mMeteor.disconnect();
            mMeteor.removeCallbacks();
        }
    }

    @Nullable
    public static MeteorSingleton getMeteorSingleton() {
        return mMeteor;
    }

}