package kz.clouddelivery.daas.models;

/**
 * Created by Olzhas on 06.01.2017.
 */

public class Courier {
    String name;
    String phone;
    String email;
    String car;

    public Courier() {
    }

    public Courier(String name, String phone, String email, String car) {
        this.name = name;
        this.phone = phone;
        this.email = email;
        this.car = car;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCar() {
        return car;
    }

    public void setCar(String car) {
        this.car = car;
    }

    @Override
    public String toString() {
        return "Courier{" +
                "name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", car='" + car + '\'' +
                '}';
    }
}
