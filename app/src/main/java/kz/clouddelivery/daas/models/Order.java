package kz.clouddelivery.daas.models;


import com.google.android.gms.maps.model.LatLng;

public class Order{

    private String id;
    private String customId;
    private String addressTo;
    private String generalPrice;
    private String planDeliverPeriod;
    private String sortId;
    private String clientId;
    private String status;
    private String statusGroup;
    private LatLng coordTo;
    private LatLng coordFrom;

    public Order() {
    }

    public Order(String id, String customId, String addressTo, String generalPrice,
                 String planDeliverPeriod, String sortId, String clientId, String status, String statusGroup, LatLng coordTo, LatLng coordFrom) {
        this.id = id;
        this.customId = customId;
        this.addressTo = addressTo;
        this.generalPrice = generalPrice;
        this.planDeliverPeriod = planDeliverPeriod;
        this.sortId = sortId;
        this.clientId = clientId;
        this.status = status;
        this.statusGroup = statusGroup;
        this.coordTo = coordTo;
        this.coordFrom = coordFrom;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id='" + id + '\'' +
                ", customId='" + customId + '\'' +
                ", addressTo='" + addressTo + '\'' +
                ", generalPrice='" + generalPrice + '\'' +
                ", planDeliverPeriod='" + planDeliverPeriod + '\'' +
                ", sortId='" + sortId + '\'' +
                ", clientId='" + clientId + '\'' +
                ", status='" + status + '\'' +
                ", statusGroup='" + statusGroup + '\'' +
                ", coordTo=" + coordTo +
                ", coordFrom=" + coordFrom +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCustomId() {
        return customId;
    }

    public void setCustomId(String customId) {
        this.customId = customId;
    }

    public String getAddressTo() {
        return addressTo;
    }

    public void setAddressTo(String addressTo) {
        this.addressTo = addressTo;
    }

    public String getGeneralPrice() {
        return generalPrice;
    }

    public void setGeneralPrice(String generalPrice) {
        this.generalPrice = generalPrice;
    }

    public String getPlanDeliverPeriod() {
        return planDeliverPeriod;
    }

    public void setPlanDeliverPeriod(String planDeliverPeriod) {
        this.planDeliverPeriod = planDeliverPeriod;
    }

    public String getSortId() {
        return sortId;
    }

    public void setSortId(String sortId) {
        this.sortId = sortId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusGroup() {
        return statusGroup;
    }

    public void setStatusGroup(String statusGroup) {
        this.statusGroup = statusGroup;
    }

    public LatLng getCoordTo() {
        return coordTo;
    }

    public void setCoordTo(LatLng coordTo) {
        this.coordTo = coordTo;
    }

    public LatLng getCoordFrom() {
        return coordFrom;
    }

    public void setCoordFrom(LatLng coordFrom) {
        this.coordFrom = coordFrom;
    }

}
