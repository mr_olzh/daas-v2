package kz.clouddelivery.daas.models;


public class Comment {
    private String id;
    private String fromName, message;
    private boolean isSelf;
    private String applicationId;

    public Comment() {
    }

    public Comment(String id, String fromName, String message, boolean isSelf) {
        this.id = id;
        this.fromName = fromName;
        this.message = message;
        this.isSelf = isSelf;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFromName() {
        return fromName;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public String getApplicationId() {
        return applicationId;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSelf() {
        return isSelf;
    }

    public void setSelf(boolean isSelf) {
        this.isSelf = isSelf;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "id='" + id + '\'' +
                ", fromName='" + fromName + '\'' +
                ", message='" + message + '\'' +
                ", isSelf=" + isSelf +
                ", applicationId='" + applicationId + '\'' +
                '}';
    }
}
