package kz.clouddelivery.daas.models;


public class Good {
    private String name, weight, id, price, volume, amount, total_price;

    public Good() {
    }

    public Good(String name, String weight, String id, String price, String volume,
                String amount, String total_price) {
        this.name = name;
        this.weight = weight;
        this.id = id;
        this.price = price;
        this.volume = volume;
        this.amount = amount;
        this.total_price = total_price;
    }

    @Override
    public String toString() {
        return "Good{" +
                "name='" + name + '\'' +
                ", weight='" + weight + '\'' +
                ", id='" + id + '\'' +
                ", price='" + price + '\'' +
                ", volume='" + volume + '\'' +
                ", amount='" + amount + '\'' +
                ", total_price='" + total_price + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTotal_price() {
        return total_price;
    }

    public void setTotal_price(String total_price) {
        this.total_price = total_price;
    }
}
