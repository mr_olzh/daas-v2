package kz.clouddelivery.daas.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import kz.clouddelivery.daas.RelogBus;
import kz.clouddelivery.daas.activities.BaseActivity;

import static kz.clouddelivery.daas.activities.BaseActivity.NetworkConnectionEvent.CONNECTED;
import static kz.clouddelivery.daas.activities.BaseActivity.NetworkConnectionEvent.DISCONNECTED;

public class NetworkConnectionReceiver extends BroadcastReceiver {
    public NetworkConnectionReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        ConnectivityManager connectivityManager = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);

        final NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

        if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
            // Connected
            RelogBus.getInstance().post(new BaseActivity.NetworkConnectionEvent(CONNECTED));
        } else {
            // Disconnected
            RelogBus.getInstance().post(new BaseActivity.NetworkConnectionEvent(DISCONNECTED));
        }
    }
}
