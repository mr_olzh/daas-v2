package kz.clouddelivery.daas.fragments;


import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import kz.clouddelivery.daas.R;
import kz.clouddelivery.daas.adapters.GoodRecyclerAdapter;
import kz.clouddelivery.daas.holders.ConstantsHolder;
import kz.clouddelivery.daas.holders.InstanceHolder;
import kz.clouddelivery.daas.models.Good;
import  im.delight.android.ddp.MeteorSingleton;
import  im.delight.android.ddp.ResultListener;

/**
 * A simple {@link Fragment} subclass.
 */
public class GoodsFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG_DESCRIPTION = "description";
    private static final String TAG_CODE = "code";
    private static final String TAG_ID = "id";
    private static final String TAG_NAME = "name";
    private static final String TAG_WEIGHT = "weight";
    private static final String TAG_PRICE = "price";
    private static final String TAG_VOLUME = "volume";
    private static final String TAG_AMOUNT = "quantity";
    private static final String TAG_TOTAL_PRICE = "totalPrice";
    private static final String TAG_GOODS_FRAGMENT = "goods fragment";
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private String id;
    private ArrayList<Good> goods = new ArrayList<>();
    private ProgressBar progressBar;

    public GoodsFragment() {
        // Required empty public constructor
    }

    public static Good getGood(JSONObject json) {
        Good good = new Good();
        try {
            good.setName(json.getString(TAG_NAME));
            good.setWeight(json.getString(TAG_WEIGHT));
            good.setId(json.getString(TAG_CODE));
            good.setPrice(json.getString(TAG_PRICE));
            good.setVolume(json.getString(TAG_VOLUME));
            good.setAmount(json.getString(TAG_AMOUNT));
            good.setTotal_price(json.getString(TAG_TOTAL_PRICE));
        } catch (JSONException | NullPointerException e) {
            e.printStackTrace();
        }

        return good;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_goods, container, false);
        setToolbarText(R.id.toolbar, getResources().getString(R.string.goods));

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);

        progressBar = (ProgressBar)view.findViewById(R.id.progressBar);

        id = getArguments().getString(TAG_ID);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);
        getAndSetGoodsList();

        return view;
    }

    private void getAndSetGoodsList() {
        progressBar.setVisibility(View.VISIBLE);
        goods.clear();
        if (!MeteorSingleton.hasInstance()) {
            InstanceHolder.createMeteorInstance(getContext());
        }
        if (MeteorSingleton.hasInstance() && MeteorSingleton.getInstance().isConnected()) {
            MeteorSingleton.getInstance().call(ConstantsHolder.CALL_GOODS, new Object[]{id}, new ResultListener() {

                @Override
                public void onSuccess(String result) {
                    String profileJson = "{" + TAG_DESCRIPTION + ":" + result + "}";
                    Log.e("good", profileJson);
                    try {
                        JSONObject jsonObj = new JSONObject(profileJson);
                        JSONArray array = jsonObj.getJSONArray(TAG_DESCRIPTION);

                        for (int i = 0; i < array.length(); i++) {
                            JSONObject goodJson = array.getJSONObject(i);
                            goods.add(getGood(goodJson));
                        }
                        progressBar.setVisibility(View.INVISIBLE);
                        setAdapter();

                    } catch (JSONException | NullPointerException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(String error, String reason, String details) {
                    Log.e(TAG_GOODS_FRAGMENT, ConstantsHolder.TAG_METEOR_RESULT_ERROR);
                    progressBar.setVisibility(View.INVISIBLE);
                }
            });
        }


    }

    private void setAdapter() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(new GoodRecyclerAdapter(goods));
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                getAndSetGoodsList();
                swipeRefreshLayout.setRefreshing(false);
            }
        }, 1000);
    }
}
