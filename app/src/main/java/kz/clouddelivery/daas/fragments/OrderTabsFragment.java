package kz.clouddelivery.daas.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import kz.clouddelivery.daas.R;
import kz.clouddelivery.daas.adapters.OrderTabsAdapter;
import kz.clouddelivery.daas.holders.ConstantsHolder;
import kz.clouddelivery.daas.holders.InstanceHolder;
import  im.delight.android.ddp.MeteorSingleton;


public class OrderTabsFragment extends BaseFragment {

    View view;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    @SuppressLint("InflateParams")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_tabs, null);
        tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        viewPager = (ViewPager) view.findViewById(R.id.viewpager);

        viewPager.setAdapter(new OrderTabsAdapter(getChildFragmentManager(), getContext()));

        tabLayout.post(new Runnable() {
            @Override
            public void run() {
                tabLayout.setupWithViewPager(viewPager);
            }
        });

        return view;

    }


}