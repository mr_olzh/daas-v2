package kz.clouddelivery.daas.fragments;


import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.LinkedHashMap;

import im.delight.android.ddp.MeteorSingleton;
import im.delight.android.ddp.db.Document;
import kz.clouddelivery.daas.R;
import kz.clouddelivery.daas.RelogBus;
import kz.clouddelivery.daas.adapters.OrderRecyclerAdapter;
import kz.clouddelivery.daas.holders.ConstantsHolder;
import kz.clouddelivery.daas.holders.FunctionsHolder;
import kz.clouddelivery.daas.models.Order;

import static kz.clouddelivery.daas.activities.MainActivity.updateBadge;

/**
 * A simple {@link Fragment} subclass.
 */
public class ActiveOrderFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG_IN_PROCESS = "inprocess";
    private static final String TAG_END = "end";
    private static final String TAG_CORDS = "coords";
    private static final String TAG_NAME = "name";
    private static final String TAG_STATUS_GROUP = "statusGroup";
    private static final String TAG_PLAN_DELIVERY_PERIOD = "planDeliverPeriod";
    private static final String TAG_ADDRESS_TO = "addressTo";
    private static final String TAG_STATUS = "status";
    private static final String TAG_CLIENT_ID = "clientId";
    private static final String TAG_SORT_ID = "sortId";
    private static final String TAG_GENERAL_PRICE = "generalPrice";
    private static final String TAG_CUSTOM_ID = "customId";

    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerView;
    private ArrayList<Order> items = new ArrayList<>();
    private ProgressBar progressBar;
    Context context;

    public ActiveOrderFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_active_order, container, false);

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);
        loadApplications();
        return view;
    }

    public void loadApplications() {
        progressBar.setVisibility(View.VISIBLE);
        items.clear();
        Document[] applicationsDocuments = null;
        try {
            if (MeteorSingleton.hasInstance())
                applicationsDocuments = MeteorSingleton.getInstance()
                        .getDatabase()
                        .getCollection(ConstantsHolder.COLLECTION_APPLICATIONS).find();
        } catch (ConcurrentModificationException ignored) {

        }

        if (applicationsDocuments != null) {
            for (Document applicationsDocument : applicationsDocuments) {
                Order order = new Order();

                try {
                    order.setId(FunctionsHolder.nullToEmpty(applicationsDocument.getId()));
                    order.setCustomId(FunctionsHolder.nullToEmpty(applicationsDocument.getField(TAG_CUSTOM_ID)));
                    order.setGeneralPrice(FunctionsHolder.nullToEmpty(applicationsDocument.getField(TAG_GENERAL_PRICE)));
                    order.setSortId(FunctionsHolder.nullToEmpty(applicationsDocument.getField(TAG_SORT_ID)));
                    order.setClientId(FunctionsHolder.nullToEmpty(applicationsDocument.getField(TAG_CLIENT_ID)));

                    Document status = MeteorSingleton.getInstance().getDatabase().getCollection(ConstantsHolder.COLLECTION_STATUSES).
                            getDocument(FunctionsHolder.nullToEmpty(applicationsDocument.getField(TAG_STATUS)));

                    if (status != null) {
                        order.setStatus(FunctionsHolder.nullToEmpty(status.getField(TAG_NAME)));
                    }
                    order.setStatusGroup(FunctionsHolder.nullToEmpty(applicationsDocument.getField(TAG_STATUS_GROUP)));

                    LinkedHashMap<String, Object> addressTo = (LinkedHashMap<String, Object>)
                            applicationsDocument.getField(TAG_ADDRESS_TO);
                    order.setAddressTo(FunctionsHolder.nullToEmpty(addressTo.get(TAG_NAME)));

//                ArrayList<Double> cordsList = (ArrayList<Double>) addressTo.get(TAG_CORDS);
//                Log.e("coords", cordsList.get(0) + "/" + cordsList.get(1));
//                order.setCoordTo(new LatLng(cordsList.get(0), cordsList.get(1)));
//
                    LinkedHashMap<String, Object> planDeliverPeriod = (LinkedHashMap<String, Object>)
                            applicationsDocument.getField(TAG_PLAN_DELIVERY_PERIOD);
                    order.setPlanDeliverPeriod(FunctionsHolder.nullToEmpty(planDeliverPeriod.get(TAG_END)));

                    if (order.getStatusGroup().equals(TAG_IN_PROCESS)) {
                        items.add(order);
                    }

                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
            updateBadge(context, items.size() + "");
            Collections.sort(items, new Comparator<Order>() {
                @Override
                public int compare(Order p1, Order p2) {
                    if (p1.getSortId() != null && p2.getSortId() != null && !p2.getSortId().equals("") && !p1.getSortId().equals("")) {
                        int number1 = Integer.parseInt(p1.getSortId());
                        int number2 = Integer.parseInt(p2.getSortId());

                        if (number1 == number2)
                            return 0;
                        else if (number1 > number2)
                            return 1;
                        else
                            return -1;
                    } else
                        return -1;
                }
            });
            setAdapter();
        }
        progressBar.setVisibility(View.INVISIBLE);

    }

    @Override
    public void onAttach(Context context) {
        this.context = context;
        super.onAttach(context);
    }

    private void setAdapter() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(new OrderRecyclerAdapter(items));
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                loadApplications();
                swipeRefreshLayout.setRefreshing(false);
            }
        }, 1000);
    }

    //do not delete this, this code need for RelogBus

    @Override
    public void onResume() {
        super.onResume();
        RelogBus.getInstance().register(this);
        loadApplications();
    }

    @Override
    public void onPause() {
        super.onPause();
        RelogBus.getInstance().unregister(this);
    }

    @Subscribe
    public void onOrdersLoaded(OrdersLoadedEvent event) {
        loadApplications();
    }

    public static class OrdersLoadedEvent {
    }
}
