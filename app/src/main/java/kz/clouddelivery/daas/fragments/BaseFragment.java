package kz.clouddelivery.daas.fragments;

import android.app.Activity;
import android.content.SharedPreferences;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import kz.clouddelivery.daas.R;

import static kz.clouddelivery.daas.holders.ConstantsHolder.EMPTY_STRING;

public abstract class BaseFragment extends Fragment {

    protected void replaceFragment(@NonNull BaseFragment fragment, boolean isStacked) {
        FragmentManager manager = getActivity().getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.container, fragment);
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        if (isStacked) {
            transaction.addToBackStack(fragment.getClass().getSimpleName());
        }
        transaction.commit();
    }

    @Nullable
    protected Toolbar setToolbarText(@IdRes int toolbarId, String text) {
        Toolbar toolbar = (Toolbar) getActivity().findViewById(toolbarId);
        setToolbarTitle(EMPTY_STRING);
        TextView toolbarTV = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbarTV.setText(text);
        return toolbar;
    }

    @Nullable
    protected ActionBar setToolbarTitle(String title) {
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(title);
        }
        return actionBar;
    }

    protected void cleanSharedPreferences(String name) {
        SharedPreferences sp = getActivity().getSharedPreferences(name, Activity.MODE_PRIVATE);
        sp.edit().clear().apply();
    }

}
