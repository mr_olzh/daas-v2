package kz.clouddelivery.daas.fragments;


import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.LinkedHashMap;
import java.util.List;

import im.delight.android.ddp.MeteorSingleton;
import im.delight.android.ddp.db.Document;
import kz.clouddelivery.daas.R;
import kz.clouddelivery.daas.RelogBus;
import kz.clouddelivery.daas.adapters.OrderPaginationAdapter;
import kz.clouddelivery.daas.custom.PaginationScrollListener;
import kz.clouddelivery.daas.holders.ConstantsHolder;
import kz.clouddelivery.daas.holders.FunctionsHolder;
import kz.clouddelivery.daas.models.Order;

/**
 * A simple {@link Fragment} subclass.
 */
public class FinishedOrderFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG_FINISHED = "finished";
    private static final String TAG_END = "end";
    private static final String TAG_CORDS = "coords";
    private static final String TAG_NAME = "name";
    private static final String TAG_STATUS_GROUP = "statusGroup";
    private static final String TAG_PLAN_DELIVERY_PERIOD = "planDeliverPeriod";
    private static final String TAG_ADDRESS_TO = "addressTo";
    private static final String TAG_STATUS = "status";
    private static final String TAG_CLIENT_ID = "clientId";
    private static final String TAG_SORT_ID = "sortId";
    private static final String TAG_GENERAL_PRICE = "generalPrice";
    private static final String TAG_CUSTOM_ID = "customId";

    private static int PAGE_START = 1;
    private static final int limit = 20;
    private int TOTAL_PAGES = 10;

    private SwipeRefreshLayout swipeRefreshLayout;
    private ArrayList<Order> mItems = new ArrayList<>();
    private OrderPaginationAdapter adapter;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int currentPage = PAGE_START;

    public FinishedOrderFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_finished_order, container, false);

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        adapter = new OrderPaginationAdapter(getActivity(), mItems);

        recyclerView.setAdapter(adapter);


        recyclerView.addOnScrollListener(new PaginationScrollListener(mLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        loadNextPage();
                    }
                }, 1000);
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
        loadFirstPage();
        return view;
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                loadApplications();
                swipeRefreshLayout.setRefreshing(false);
            }
        }, 1000);
    }

    public ArrayList<Order> loadApplications() {
        ArrayList<Order> items = new ArrayList<>();
        Document[] applicationsDocuments = null;
        try {
            if (MeteorSingleton.hasInstance())
                applicationsDocuments = MeteorSingleton.getInstance()
                        .getDatabase()
                        .getCollection(ConstantsHolder.COLLECTION_APPLICATIONS).find();
        } catch (ConcurrentModificationException ignored) {

        }

        if (applicationsDocuments != null) {

            for (Document applicationsDocument : applicationsDocuments) {
                Order order = new Order();
                try {
                    order.setId(FunctionsHolder.nullToEmpty(applicationsDocument.getId()));
                    order.setCustomId(FunctionsHolder.nullToEmpty(applicationsDocument.getField(TAG_CUSTOM_ID)));
                    order.setGeneralPrice(FunctionsHolder.nullToEmpty(applicationsDocument.getField(TAG_GENERAL_PRICE) == null ? "0" : applicationsDocument.getField(TAG_GENERAL_PRICE)));
                    order.setSortId(FunctionsHolder.nullToEmpty(applicationsDocument.getField(TAG_SORT_ID)));
                    order.setClientId(FunctionsHolder.nullToEmpty(applicationsDocument.getField(TAG_CLIENT_ID)));

                    Document status = MeteorSingleton.getInstance().getDatabase().getCollection(ConstantsHolder.COLLECTION_STATUSES).
                            getDocument(FunctionsHolder.nullToEmpty(applicationsDocument.getField(TAG_STATUS)));

                    if (status != null) {
                        order.setStatus(FunctionsHolder.nullToEmpty(status.getField(TAG_NAME)));
                    }
                    order.setStatusGroup(FunctionsHolder.nullToEmpty(applicationsDocument.getField(TAG_STATUS_GROUP)));

                    LinkedHashMap<String, Object> addressTo = (LinkedHashMap<String, Object>)
                            applicationsDocument.getField(TAG_ADDRESS_TO);
                    order.setAddressTo(FunctionsHolder.nullToEmpty(addressTo.get(TAG_NAME)));

//                ArrayList<Double> cordsList = (ArrayList<Double>) addressTo.get(TAG_CORDS);
//                order.setCoordTo(new LatLng(cordsList.get(0), cordsList.get(1)));

                    LinkedHashMap<String, Object> planDeliverPeriod = (LinkedHashMap<String, Object>)
                            applicationsDocument.getField(TAG_PLAN_DELIVERY_PERIOD);
                    order.setPlanDeliverPeriod(FunctionsHolder.nullToEmpty(planDeliverPeriod.get(TAG_END)));

                    if (order.getStatusGroup().equals(TAG_FINISHED)) {
                        items.add(order);
                    }

                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                Collections.sort(items, new Comparator<Order>() {
                    @Override
                    public int compare(Order p1, Order p2) {
                        if (p1.getSortId() != null && p2.getSortId() != null && !p1.getSortId().equals("") && !p2.getSortId().equals("")) {
                            int number1 = Integer.parseInt(p1.getSortId());
                            int number2 = Integer.parseInt(p2.getSortId());

                            if (number1 == number2)
                                return 0;
                            else if (number1 > number2)
                                return 1;
                            else
                                return -1;
                        } else
                            return -1;
                    }
                });
            }
        }
        return items;
    }

    private void loadFirstPage() {
        adapter.clear();
        PAGE_START = 1;
        List<Order> newItems = loadApplications();
        adapter.addAll(newItems);

        if (currentPage <= TOTAL_PAGES && newItems.size() != 0 &&
                newItems.size() == limit * currentPage) {
            adapter.addLoadingFooter();
        } else {
            isLastPage = true;
        }
    }

    private void loadNextPage() {
        MeteorSingleton.getInstance().unsubscribe(FunctionsHolder.getApplicationsMobileSubscribeId(getActivity()));
        String subscriptionId = MeteorSingleton.getInstance().subscribe(ConstantsHolder.SUBSCRIBE_APPLICATION,
                new Object[]{currentPage * limit});
        FunctionsHolder.saveApplicationsMobileSubscribeId(getActivity(), subscriptionId);

        //Log.e("SUBSCRIBE", "Finished_APPLICATION");
        List<Order> newItems = loadApplications();
        adapter.removeLoadingFooter();
        isLoading = false;
        adapter.addAll(newItems);
        if (currentPage <= TOTAL_PAGES && newItems.size() != 0 &&
                newItems.size() == limit * currentPage) {
            adapter.addLoadingFooter();
        } else {
            isLastPage = true;
        }
    }

    //RelogBus
    @Override
    public void onResume() {
        super.onResume();
        RelogBus.getInstance().register(this);
        loadFirstPage();
        //Log.e("Meteor", "Finished_reload");
    }

    @Override
    public void onPause() {
        super.onPause();
        RelogBus.getInstance().unregister(this);
    }

    @Subscribe
    public void onOrdersLoaded(ActiveOrderFragment.OrdersLoadedEvent event) {
        //Log.e("Meteor", "Finished_reload");
        loadFirstPage();
    }

    public static class OrdersLoadedEvent {
    }
}