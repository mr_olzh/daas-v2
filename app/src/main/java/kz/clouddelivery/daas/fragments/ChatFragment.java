package kz.clouddelivery.daas.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

import im.delight.android.ddp.MeteorSingleton;
import im.delight.android.ddp.ResultListener;
import im.delight.android.ddp.db.Document;
import kz.clouddelivery.daas.R;
import kz.clouddelivery.daas.RelogBus;
import kz.clouddelivery.daas.adapters.MessagesListAdapter;
import kz.clouddelivery.daas.holders.ConstantsHolder;
import kz.clouddelivery.daas.models.Comment;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChatFragment extends BaseFragment implements View.OnClickListener {

    private static final String DOCUMENT_ID_KEY = "document_id";
    private static final String TAG_COMMENTS = "comments";
    private static final String TAG_APPLICATION_ID = "applicationId";
    private static final String TAG_BODY = "body";
    private static final String TAG_AUTHOR = "author";
    private static final String TAG_ID = "id";
    private static final String TAG_COURIER = "isCourier";

    private static List<Comment> commentsList = new ArrayList<>();
    private static MessagesListAdapter adapter;
    private static String id;
    private EditText messageEditText;

    public ChatFragment() {
        // Required empty public constructor
    }

    public static ChatFragment newInstance(String documentId) {
        ChatFragment fragment = new ChatFragment();
        Bundle args = new Bundle();
        args.putString(DOCUMENT_ID_KEY, documentId);
        fragment.setArguments(args);

        return fragment;
    }

    public static Comment getComment(Document document) {
        Comment comment = new Comment();
        comment.setId(document.getId());
        comment.setMessage(document.getField(TAG_BODY).toString());
        comment.setFromName(document.getField(TAG_AUTHOR).toString());
        comment.setSelf(Boolean.parseBoolean(document.getField(TAG_COURIER).toString()));
        comment.setApplicationId(document.getField(TAG_APPLICATION_ID).toString());
        return comment;
    }

    public static void updateCommentsAdapter() {
        Document[] commentsDocuments = MeteorSingleton.getInstance()
                .getDatabase()
                .getCollection(TAG_COMMENTS)
                .whereEqual(TAG_APPLICATION_ID, id)
                .find();

        ArrayList<Comment> listMessagesTemp = new ArrayList<>();

        for (Document commentsDocument : commentsDocuments) {
            listMessagesTemp.add(getComment(commentsDocument));
        }

        int newCommentsCount = listMessagesTemp.size() - commentsList.size();

        if (newCommentsCount <= 0) {
            return;
        }

        int startIndex = commentsList.size();

        for (int i = startIndex; i < listMessagesTemp.size(); i++) {
            Comment comment = listMessagesTemp.get(i);
            commentsList.add(comment);
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chat, container, false);

        setToolbarText(R.id.toolbar, getResources().getString(R.string.chat));

        ImageView sendImageView = (ImageView) view.findViewById(R.id.sendButton);
        sendImageView.setOnClickListener(this);

        ListView commentsListView = (ListView) view.findViewById(R.id.messagesListView);

        messageEditText = (EditText) view.findViewById(R.id.inputEditText);
        id = getArguments().getString(TAG_ID);

        if (MeteorSingleton.hasInstance() && MeteorSingleton.getInstance().isConnected()) {
            MeteorSingleton.getInstance().subscribe(ConstantsHolder.SUBSCRIBE_COMMENTS, new Object[]{id});
        }

        Document[] commentsDocuments = MeteorSingleton.getInstance()
                .getDatabase()
                .getCollection(TAG_COMMENTS)
                .whereEqual(TAG_APPLICATION_ID, id)
                .find();

        commentsList.clear();
        for (Document commentsDocument : commentsDocuments) {
            commentsList.add(getComment(commentsDocument));
        }

        adapter = new MessagesListAdapter(getActivity(), commentsList);
        commentsListView.setAdapter(adapter);

        return view;
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.sendButton) {
            sendMessage();
        }
    }

    private void sendMessage() {
        String text = messageEditText.getText().toString().trim();
        if (!text.isEmpty()) {
            MeteorSingleton.getInstance().call(ConstantsHolder.CALL_SEND_COMMENT,
                    new Object[]{id, text}, new ResultListener() {
                        @Override
                        public void onSuccess(String result) {
                            updateCommentsAdapter();
                        }

                        @Override
                        public void onError(String error, String reason, String details) {

                        }
                    });
            messageEditText.setText("");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        RelogBus.getInstance().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        RelogBus.getInstance().unregister(this);
    }

    @Override
    public void onDestroy() {
        MeteorSingleton.getInstance().unsubscribe(ConstantsHolder.SUBSCRIBE_COMMENTS);
        super.onDestroy();
    }

    @Subscribe
    public void onCommentsLoaded(ChatFragment.CommentsLoadedEvent event) {
        updateCommentsAdapter();
    }

    public static class CommentsLoadedEvent {
    }

}
