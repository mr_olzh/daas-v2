package kz.clouddelivery.daas.fragments;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.ui.IconGenerator;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import im.delight.android.ddp.MeteorSingleton;
import im.delight.android.ddp.db.Document;
import kz.clouddelivery.daas.R;
import kz.clouddelivery.daas.activities.OrderActivity;
import kz.clouddelivery.daas.holders.ConstantsHolder;
import kz.clouddelivery.daas.holders.FunctionsHolder;
import kz.clouddelivery.daas.models.Order;

/**
 * A simple {@link Fragment} subclass.
 */
public class RoutesFragment extends BaseFragment implements OnMapReadyCallback {

    private static final int MY_PERMISSION_ACCESS_FINE_LOCATION = 99;

    private static final String TAG_PRODUCT_ID = "id";
    private static final String TAG_APPLICATIONS = "applications";
    private static final String TAG_IN_PROCESS = "inprocess";
    private static final String TAG_END = "end";
    private static final String TAG_CORDS = "coords";
    private static final String TAG_NAME = "name";
    private static final String TAG_STATUS_GROUP = "statusGroup";
    private static final String TAG_PLAN_DELIVERY_PERIOD = "planDeliverPeriod";
    private static final String TAG_ADDRESS_TO = "addressTo";
    private static final String TAG_STATUS = "status";
    private static final String TAG_CLIENT_ID = "clientId";
    private static final String TAG_SORT_ID = "sortId";
    private static final String TAG_GENERAL_PRICE = "generalPrice";
    private static final String TAG_CUSTOM_ID = "customId";

    private MapView mMapView;
    private ArrayList<Order> items = new ArrayList<>();
    private GoogleMap mGoogleMap;
    private ProgressBar progressBar;

    public RoutesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_routes, container, false);

        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        mMapView = (MapView) view.findViewById(R.id.mapView);
        if (mMapView != null) {
            mMapView.onCreate(savedInstanceState);
            mMapView.onResume();// needed to get the map to display immediately
        }
        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(this);

        loadApplications();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mMapView != null)
            mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mMapView != null)
            mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if (mMapView != null)
            mMapView.onLowMemory();
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;

        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            getContext(), R.raw.map_style));

            if (!success) {
                Log.e("map", "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e("Map", "Can't find style. Error: ", e);
        }

        checkAndSetLocation(googleMap);
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.getUiSettings().setAllGesturesEnabled(true);
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        googleMap.getUiSettings().setCompassEnabled(true);
        googleMap.getUiSettings().setMapToolbarEnabled(true);


        for (int i = 0; i < items.size(); i++) {
            IconGenerator tc = new IconGenerator(getActivity());
            tc.setTextAppearance(R.style.iconGenText);
            tc.setColor(ContextCompat.getColor(getContext(), R.color.colorAccent));

            MarkerOptions options = new MarkerOptions();
            Bitmap bmp = tc.makeIcon((i + 1) + "");

            googleMap.addMarker(options.title(i + "").snippet("")
                    .position(items.get(i).getCoordTo()).icon(BitmapDescriptorFactory.fromBitmap(bmp)));
        }

        googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                try {
                    int position = Integer.parseInt(marker.getTitle());
                    if (position > 0 && items.size() > position) {
                        Intent intent = new Intent(getActivity(), OrderActivity.class);
                        intent.putExtra(TAG_PRODUCT_ID, items.get(position).getId());
                        getActivity().startActivity(intent);
                    }
                } catch (Exception ignored) {

                }
                return true;
            }
        });

        if (items.size() != 0) {
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(items.get(0).getCoordTo()).zoom(12).build();
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }

    }

    private void checkAndSetLocation(GoogleMap googleMap) {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            googleMap.setMyLocationEnabled(true);
        } else {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSION_ACCESS_FINE_LOCATION);
        }
    }

    public void loadApplications() {
        progressBar.setVisibility(View.VISIBLE);
        items.clear();
        Document[] applicationsDocuments = MeteorSingleton.getInstance()
                .getDatabase()
                .getCollection(TAG_APPLICATIONS).find();

        for (Document applicationsDocument : applicationsDocuments) {
            Order order = new Order();
            try {
                order.setId(FunctionsHolder.nullToEmpty(applicationsDocument.getId()));
                order.setCustomId(FunctionsHolder.nullToEmpty(applicationsDocument.getField(TAG_CUSTOM_ID)));
                order.setGeneralPrice(FunctionsHolder.nullToEmpty(applicationsDocument.getField(TAG_GENERAL_PRICE)));
                order.setSortId(FunctionsHolder.nullToEmpty(applicationsDocument.getField(TAG_SORT_ID)));
                order.setClientId(FunctionsHolder.nullToEmpty(applicationsDocument.getField(TAG_CLIENT_ID)));

                Document status = MeteorSingleton.getInstance().getDatabase().getCollection(ConstantsHolder.COLLECTION_STATUSES).
                        getDocument(FunctionsHolder.nullToEmpty(applicationsDocument.getField(TAG_STATUS)));

                if (status != null) {
                    order.setStatus(FunctionsHolder.nullToEmpty(status.getField(TAG_NAME)));
                }
                order.setStatusGroup(FunctionsHolder.nullToEmpty(applicationsDocument.getField(TAG_STATUS_GROUP)));

                LinkedHashMap<String, Object> addressTo = (LinkedHashMap<String, Object>)
                        applicationsDocument.getField(TAG_ADDRESS_TO);
                order.setAddressTo(FunctionsHolder.nullToEmpty(addressTo.get(TAG_NAME)));

                try {
                    ArrayList<Double> cordsList = (ArrayList<Double>) addressTo.get(TAG_CORDS);
                    Log.e("cords", cordsList.get(0) + "/" + cordsList.get(1));
                    order.setCoordTo(new LatLng(cordsList.get(0), cordsList.get(1)));
                } catch (ClassCastException e) {
                    continue;
                }

                LinkedHashMap<String, Object> planDeliverPeriod = (LinkedHashMap<String, Object>)
                        applicationsDocument.getField(TAG_PLAN_DELIVERY_PERIOD);
                order.setPlanDeliverPeriod(FunctionsHolder.nullToEmpty(planDeliverPeriod.get(TAG_END)));

                //Log.e("Order", order.toString());
                if (order.getStatusGroup().equals(TAG_IN_PROCESS)) {
                    items.add(order);
                }

            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }
        progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == MY_PERMISSION_ACCESS_FINE_LOCATION) {
            if (permissions.length == 1 &&
                    permissions[0] == Manifest.permission.ACCESS_FINE_LOCATION &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {
                    mGoogleMap.setMyLocationEnabled(true);
                }
            }
        }
    }
}