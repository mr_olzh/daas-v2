package kz.clouddelivery.daas.fragments;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.ui.IconGenerator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import im.delight.android.ddp.MeteorSingleton;
import im.delight.android.ddp.ResultListener;
import im.delight.android.ddp.db.Document;
import kz.clouddelivery.daas.R;
import kz.clouddelivery.daas.activities.BaseActivity;
import kz.clouddelivery.daas.adapters.HorizontalAdapter;
import kz.clouddelivery.daas.custom.MapLocationListener;
import kz.clouddelivery.daas.holders.ConstantsHolder;
import kz.clouddelivery.daas.holders.FunctionsHolder;
import kz.clouddelivery.daas.holders.GoogleApiHolder;
import kz.clouddelivery.daas.holders.YandexNavigatorHolder;

/**
 * A simple {@link Fragment} subclass.
 */

public class OrderDetailsFragment extends BaseFragment implements View.OnClickListener,
        OnMapReadyCallback {

    private static final String ID_KEY = "id";
    private static final String TAG_DESCRIPTION = "description";
    private static final int CAMERA_REQUEST = 100;
    private static final String CALL_SEND_IMAGE = "sendImage";
    private static final String TAG_TELEPHONE = "tel:";
    private static final String TAG_DATA = "data";
    private static final String TAG_NAME = "name";
    private static final String TAG_ERROR = "error";
    private static final String TAG_STATUS = "status";
    private static final String TAG_COORDINATES = "coords";
    private static final String TAG_IMAGES_COUNT = "imagesCount";
    private static final String TAG_PHONE = "phone1";
    private static final String TAG_CLIENT = "client";
    private static final String TAG_END = "end";
    private static final String TAG_DELIVERY_PERIOD = "planDeliverPeriod";
    private static final String TAG_DETAILS = "details";
    private static final String TAG_ADDRESS = "address";
    private static final String TAG_ADDRESS_FROM = "addressFrom";
    private static final String TAG_ADDRESS_TO = "addressTo";
    private static final String TAG_CLIENT_NAME = "clientName";
    private static final String TAG_ADDITIONAL_DETAILS = "additionalDetails";
    private static final String TAG_GENERAL_PRICE = "generalPrice";
    private static final String TAG_CUSTOM_ID = "customId";
    private static final String TAG_GOODS_COUNT = "goodsCount";
    private static final String TAG_COMMENTS_COUNT = "commentsCount";
    private static final String TAG_ORDER_DETAILS_FRAGMENT = "Order details fragment";
    private static final String TAG_APP_PRICE = "appPrice";
    private static final String TAB_DELIVERY_PRICE = "deliveryPrice";
    private static final String TAG_PAYMENT_TYPE = "paymentType";
    private ImageView statusImageView;
    private RecyclerView galleryRecyclerView;
    private LinearLayoutManager horizontalLayoutManager;
    private LatLng fromPosition, toPosition;
    private TextView statusTextView, numberTextView, addressFromTextView, addressFromDetailsTextView,
            addressToTextView, addressToDetailsTextView, deliveryTimeTextView, getTimeTextView, clientTextView, phoneTextView,
            goodsTextView, goodPriceTextView, chatTextView, imagesTextView, additionsTextView, paymentTypeTextView,
            deliveryPriceTextView, totalPriceTextView, agentTextView, contrPhoneTextView;
    private Button sendButton;
    private ScrollView scrollView;
    private MapView mapView;
    private GoogleMap googleMap;
    private String status, id;
    private String[] choices, choicesSend;
    private int selectedPosition = 0;
    private HashMap<String, Integer> statusHashMapOrder = new HashMap<>();
    private ProgressBar progressBar;
    private Context context;

    public OrderDetailsFragment() {
        // Required empty public constructor
    }

    public static OrderDetailsFragment newInstance(String documentId) {
        OrderDetailsFragment fragment = new OrderDetailsFragment();
        Bundle args = new Bundle();
        args.putString(ID_KEY, documentId);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_order_details, container, false);

        setToolbarText(R.id.toolbar, getString(R.string.order_details));

        id = getArguments().getString(ID_KEY);
        context = getActivity();

        statusTextView = (TextView) view.findViewById(R.id.statusTextView);
        paymentTypeTextView = (TextView) view.findViewById(R.id.paymentTypeTextView);
        goodPriceTextView = (TextView) view.findViewById(R.id.goodsPriceTextView);
        totalPriceTextView = (TextView) view.findViewById(R.id.totalPriceTextView);
        deliveryPriceTextView = (TextView) view.findViewById(R.id.deliveryPriceTextView);
        getTimeTextView = (TextView) view.findViewById(R.id.getTimeTextView);
        agentTextView = (TextView) view.findViewById(R.id.agentTextView);

        numberTextView = (TextView) view.findViewById(R.id.numberTextView);
        addressFromTextView = (TextView) view.findViewById(R.id.addressFromTextView);
        addressFromDetailsTextView = (TextView) view.findViewById(R.id.addressFromDetailsTextView);

        addressToTextView = (TextView) view.findViewById(R.id.addressToTextView);

        addressToDetailsTextView = (TextView) view.findViewById(R.id.addressToDetailsTextView);
        deliveryTimeTextView = (TextView) view.findViewById(R.id.timeTextView);

        clientTextView = (TextView) view.findViewById(R.id.clientTextView);
        phoneTextView = (TextView) view.findViewById(R.id.phoneTextView);
        goodsTextView = (TextView) view.findViewById(R.id.goodsTextView);

        chatTextView = (TextView) view.findViewById(R.id.chatTextView);
        imagesTextView = (TextView) view.findViewById(R.id.imagesTextView);

        additionsTextView = (TextView) view.findViewById(R.id.additionsTextView);

        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        contrPhoneTextView = (TextView)view.findViewById(R.id.contrPhoneTextView);

        statusImageView = (ImageView) view.findViewById(R.id.statusImageView);
        statusImageView.setOnClickListener(this);

        sendButton = (Button) view.findViewById(R.id.sendButton);
        sendButton.setOnClickListener(this);

        ImageView callContrImageView = (ImageView)view.findViewById(R.id.contrCallImageView);
        callContrImageView.setOnClickListener(this);

        ImageView chatImageView = (ImageView) view.findViewById(R.id.chatImageView);
        chatImageView.setOnClickListener(this);

        ImageView goodsImageView = (ImageView) view.findViewById(R.id.goodsImageView);
        goodsImageView.setOnClickListener(this);

        ImageView callImageView = (ImageView) view.findViewById(R.id.callImageView);
        callImageView.setOnClickListener(this);

        ImageView addressImageView = (ImageView) view.findViewById(R.id.addressImageView);
        addressImageView.setOnClickListener(this);

        ImageView stockAddressImageView = (ImageView) view.findViewById(R.id.stockAddressImageView);
        stockAddressImageView.setOnClickListener(this);

        ImageView cameraImageView = (ImageView) view.findViewById(R.id.photoImageView);
        cameraImageView.setOnClickListener(this);

        scrollView = (ScrollView) view.findViewById(R.id.scrollView);

        mapView = (MapView) view.findViewById(R.id.mapView);
        if (mapView != null) {
            mapView.onCreate(savedInstanceState);
            mapView.getMapAsync(this);
        }

        mapView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_MOVE:
                        scrollView.requestDisallowInterceptTouchEvent(true);
                        break;
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_CANCEL:
                        scrollView.requestDisallowInterceptTouchEvent(false);
                        break;
                }
                return mapView.onTouchEvent(event);
            }
        });

        galleryRecyclerView = (RecyclerView) view.findViewById(R.id.gallery_recycler_view);
        horizontalLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);

        getImages();
        getOrderInfo(id);
        setupStatusesAndGroups();

        return view;
    }

    private void getOrderInfo(String id) {
        if (MeteorSingleton.hasInstance() && MeteorSingleton.getInstance().isConnected()) {
            MeteorSingleton.getInstance().call(ConstantsHolder.CALL_GET_APP_BY_ID_MOBILE, new Object[]{id}, new ResultListener() {

                @SuppressLint("SetTextI18n")
                @Override
                public void onSuccess(String result) {
                    String profileJson = "{" + TAG_DESCRIPTION + ":" + result + "}";
                    Log.e("Order Info Json", profileJson);
                    try {
                        JSONObject jsonObj = new JSONObject(profileJson);
                        JSONObject info = jsonObj.getJSONObject(TAG_DESCRIPTION);
                        Log.e(TAG_ORDER_DETAILS_FRAGMENT, info.toString());

                        if (info.has(TAG_COMMENTS_COUNT))
                            chatTextView.setText(info.getString(TAG_COMMENTS_COUNT));

                        if (info.has(TAG_GOODS_COUNT))
                            goodsTextView.setText(info.getString(TAG_GOODS_COUNT));

                        if (info.has(TAG_CUSTOM_ID))
                            numberTextView.setText(info.getString(TAG_CUSTOM_ID));

                        if (info.has(TAG_GENERAL_PRICE))
                            goodPriceTextView.setText(info.getString(TAG_GENERAL_PRICE));

                        if (info.has(TAG_ADDITIONAL_DETAILS))
                            additionsTextView.setText(info.getString(TAG_ADDITIONAL_DETAILS));

                        if (info.has(TAG_CLIENT) && !info.getString(TAG_CLIENT).equals("null")) {
                            JSONObject client = info.getJSONObject(TAG_CLIENT);
                            if (client.has("name") && clientTextView != null)
                                clientTextView.setText(client.getString("name"));

                            phoneTextView.setText(client.getString(TAG_PHONE));
                        }

                        if (info.has(TAG_ADDRESS_TO)) {
                            JSONObject addressTo = info.getJSONObject(TAG_ADDRESS_TO);
                            addressToTextView.setText(addressTo.getString(TAG_NAME));
                            addressToDetailsTextView.setText(addressTo.getString(TAG_DETAILS));

                            JSONArray addressToCoords = addressTo.getJSONArray(TAG_COORDINATES);
                            toPosition = new LatLng(addressToCoords.getDouble(0), addressToCoords.getDouble(1));
                        }

                        if (info.has(TAG_ADDRESS_FROM)) {
                            JSONObject addressFrom = info.getJSONObject(TAG_ADDRESS_FROM);
                            if (addressFrom.has(TAG_ADDRESS)) {
                                addressFromTextView.setText(addressFrom.getString(TAG_ADDRESS));
                            } else {
                                addressFromTextView.setText(addressFrom.getString(TAG_NAME));
                            }
                            if (addressFrom.has("description")) {
                                addressFromDetailsTextView.setText(addressFrom.getString("description"));
                            }
                            JSONArray addressFromCoords = addressFrom.getJSONArray(TAG_COORDINATES);
                            fromPosition = new LatLng(addressFromCoords.getDouble(0), addressFromCoords.getDouble(1));
                        }

                        if (info.has("createdBy")) {
                            agentTextView.setText(info.getString("createdBy"));
                        } else {
                            agentTextView.setText("База");
                        }

                        if (info.has(TAG_DELIVERY_PERIOD)) {
                            JSONObject planDeliverPeriod = info.getJSONObject(TAG_DELIVERY_PERIOD);
                            deliveryTimeTextView.setText(planDeliverPeriod.getString(TAG_END));
                            getTimeTextView.setText(planDeliverPeriod.getString("start"));
                        }

                        if(info.has("contractorPhone")){
                            contrPhoneTextView.setText(info.getString("contractorPhone"));
                        }


                        if (info.has(TAG_STATUS))
                            statusTextView.setText(choices[statusHashMapOrder.get(info.getString(TAG_STATUS))]);

                        if (info.has(TAG_IMAGES_COUNT))
                            imagesTextView.setText(info.getString(TAG_IMAGES_COUNT));

                        if (info.getString("statusGroup").equals("finished")) {
                            sendButton.setVisibility(View.INVISIBLE);
                            statusImageView.setVisibility(View.INVISIBLE);
                        }

                        if (info.has(TAG_STATUS)) {
                            String statusId = info.getString(TAG_STATUS);
                            if (choices[statusHashMapOrder.get(statusId)].toLowerCase().equals("активные")) {
                                sendButton.setVisibility(View.VISIBLE);
                            }
                        }

                        String appPrice = "0", deliveryPrice = "0";
                        if (info.has(TAG_APP_PRICE)) {
                            appPrice = info.getString(TAG_APP_PRICE);
                            goodPriceTextView.setText(appPrice);
                        }

                        if (info.has(TAB_DELIVERY_PRICE)) {
                            deliveryPrice = info.getString(TAB_DELIVERY_PRICE);
                            deliveryPriceTextView.setText(deliveryPrice);
                        }

                        if (info.has(TAG_GENERAL_PRICE)) {
                            String generalPrice = info.getString(TAG_GENERAL_PRICE);
                            if (!generalPrice.equals("null")) {
                                totalPriceTextView.setText(info.getString(TAG_GENERAL_PRICE));
                            } else if (deliveryPrice != null && appPrice != null && appPrice.trim().length() != 0 && deliveryPrice.trim().length() != 0) {
                                totalPriceTextView.setText((Integer.parseInt(appPrice) + Integer.parseInt(deliveryPrice)) + "");
                            }
                        }

                        if (info.has(TAG_PAYMENT_TYPE)) {
                            String paymentTypeStr = info.getString(TAG_PAYMENT_TYPE);

                            switch (paymentTypeStr) {
                                case "cash":
                                    paymentTypeStr = "наличные";
                                    break;
                                case "cashless":
                                    paymentTypeStr = "безналичные";
                                    goodPriceTextView.setTextColor(ContextCompat.getColor(getActivity(), android.R.color.holo_red_dark));
                                    deliveryPriceTextView.setTextColor(ContextCompat.getColor(getActivity(), android.R.color.holo_red_dark));
                                    totalPriceTextView.setText("0");
                                    break;
                                case "ransom":
                                    paymentTypeStr = "сбор";
                                    deliveryPriceTextView.setTextColor(ContextCompat.getColor(getActivity(), android.R.color.holo_red_dark));
                                    totalPriceTextView.setText(appPrice);
                                    break;
                                case "bonus":
                                    paymentTypeStr = "бонус";
                                    goodPriceTextView.setTextColor(ContextCompat.getColor(getActivity(), android.R.color.holo_red_dark));
                                    totalPriceTextView.setText(deliveryPrice);
                                    break;
                                default:
                                    paymentTypeStr = "";
                                    break;
                            }
                            paymentTypeTextView.setText(paymentTypeStr);
                        }
                        progressBar.setVisibility(View.INVISIBLE);
                    } catch (JSONException | NullPointerException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(String error, String reason, String details) {
                    Log.e(TAG_ORDER_DETAILS_FRAGMENT, ConstantsHolder.TAG_METEOR_RESULT_ERROR);
                    progressBar.setVisibility(View.INVISIBLE);
                }
            });
        }
    }


    @Override
    public void onClick(View v) {
        int viewId = v.getId();
        Bundle bundle = getArguments();
        final Context context = v.getContext();
        switch (viewId) {
            case R.id.chatImageView:
                ChatFragment chatFragment = new ChatFragment();
                chatFragment.setArguments(bundle);
                replaceFragment(chatFragment, true);
                break;
            case R.id.goodsImageView:
                if (!goodsTextView.getText().toString().trim().equals("0")) {
                    GoodsFragment goodsFragment = new GoodsFragment();
                    goodsFragment.setArguments(bundle);
                    replaceFragment(goodsFragment, true);
                }
                break;
            case R.id.callImageView:
                String number = phoneTextView.getText().toString().trim();
                if (number.length() != 0) {
                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                    callIntent.setData(Uri.parse(TAG_TELEPHONE + number));
                    context.startActivity(callIntent);
                }
                break;

            case  R.id.contrCallImageView:
                String contrNumber = contrPhoneTextView.getText().toString().trim();
                if (contrNumber.length() != 0) {
                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                    callIntent.setData(Uri.parse(TAG_TELEPHONE + contrNumber));
                    context.startActivity(callIntent);
                }
                break;
            case R.id.addressImageView:
                if (fromPosition != null && MapLocationListener.lastPosition != null) {
                    new YandexNavigatorHolder(getActivity(), new LatLng(
                            MapLocationListener.lastPosition.latitude,
                            MapLocationListener.lastPosition.longitude),
                            fromPosition);
                }
                break;
            case R.id.stockAddressImageView:
                if (toPosition != null && MapLocationListener.lastPosition != null) {
                    new YandexNavigatorHolder(getActivity(), new LatLng(
                            MapLocationListener.lastPosition.latitude,
                            MapLocationListener.lastPosition.longitude),
                            toPosition);
                }
                break;

            case R.id.photoImageView:
                openCamera();
                break;

            case R.id.sendButton:
                if (MeteorSingleton.getInstance().isConnected() && MapLocationListener.lastPosition != null) {
                    MeteorSingleton.getInstance().call(ConstantsHolder.CALL_COURIER_START, new Object[]{
                            id, MapLocationListener.lastPosition.latitude,
                            MapLocationListener.lastPosition.longitude}, new ResultListener() {
                        @Override
                        public void onSuccess(String result) {
                            sendButton.setVisibility(View.INVISIBLE);
                            if (context != null)
                                Toast.makeText(context, R.string.request_sent, Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onError(String error, String reason, String details) {
                            Log.e(TAG_ERROR, error + " " + reason + " " + details);
                        }
                    });
                }
                break;
            case R.id.statusImageView:
                if (choices.length != 0) {
                    showStatusSelectionDialog(getContext(), getString(R.string.select));
                }
                break;
            default:
                break;
        }
    }


    @Override
    public void onResume() {
        if (mapView != null)
            mapView.onResume();
        if (MeteorSingleton.hasInstance() && !MeteorSingleton.getInstance().isConnected()
                && !MeteorSingleton.getInstance().isLoggedIn()) {
            FunctionsHolder.restartApplication((BaseActivity) getActivity());
        }
        getOrderInfo(id);
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mapView != null)
            mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if (mapView != null)
            mapView.onLowMemory();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            getContext(), R.raw.map_style));

            if (!success) {
                Log.e("map", "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e("Map", "Can't find style. Error: ", e);
        }
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        googleMap.setMyLocationEnabled(true);
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.getUiSettings().setAllGesturesEnabled(true);
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        googleMap.getUiSettings().setCompassEnabled(true);
        googleMap.getUiSettings().setMapToolbarEnabled(true);

        buildMapPath(googleMap);
    }

    private void buildMapPath(final GoogleMap map) {
        Context context = getActivity();
        if (context != null) {
            IconGenerator tc = new IconGenerator(context);
            tc.setTextAppearance(R.style.iconGenText);
            tc.setColor(Color.YELLOW);

            MarkerOptions options = new MarkerOptions();

            if (fromPosition != null && toPosition != null) {
                Bitmap bmpA = tc.makeIcon("A");
                map.addMarker(options.title(getString(R.string.from)).snippet("")
                        .position(fromPosition).icon(BitmapDescriptorFactory.fromBitmap(bmpA)));

                Bitmap bmpB = tc.makeIcon("B");
                map.addMarker(options.title(getString(R.string.to)).snippet("")
                        .position(toPosition).icon(BitmapDescriptorFactory.fromBitmap(bmpB)));

                if (toPosition != null && fromPosition != null) {
                    if (MeteorSingleton.getInstance().isConnected()) {
                        String url = GoogleApiHolder.getDirectionsUrl(toPosition, fromPosition);
                        new GoogleApiHolder().download(url, googleMap);
                    }
                }
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(fromPosition, 13);
                googleMap.animateCamera(cameraUpdate);
            } else {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        buildMapPath(map);
                    }
                }, 1000);
            }
        }
    }

    private void setupStatusesAndGroups() {
        if (MeteorSingleton.hasInstance() && MeteorSingleton.getInstance().isConnected()) {
            Document statuses[] = MeteorSingleton.getInstance().getDatabase().
                    getCollection(ConstantsHolder.COLLECTION_STATUSES).find();
            choices = new String[statuses.length];
            choicesSend = new String[statuses.length];
            statusHashMapOrder.clear();

            for (int i = 0; i < statuses.length; i++) {
                Document status = statuses[i];
                String statusName = status.getField(TAG_NAME).toString();
                String statusId = status.getId();

                choices[i] = statusName;
                choicesSend[i] = statusId;

                statusHashMapOrder.put(statusId, i);
            }
        }
    }

    private void showStatusSelectionDialog(final Context context, String title) {
        AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);

        int item_number = 0;
        if (statusHashMapOrder.containsKey(status)) {
            item_number = statusHashMapOrder.get(status);
        }

        builder.setTitle(title)
                .setSingleChoiceItems(choices, item_number, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        selectedPosition = arg1;
                    }

                })
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialog, int i) {
                        if (choices[selectedPosition].equals(statusTextView.getText().toString()) && context != null)
                            Toast.makeText(context, "Выбран текуший статус", Toast.LENGTH_SHORT).show();
                        else
                            changeStatus();
                    }
                }).setCancelable(false).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }).show();
    }

    private void changeStatus() {
        if (MeteorSingleton.getInstance().isConnected()) {

            MeteorSingleton.getInstance().call(ConstantsHolder.CALL_CHANGE_APP_STATUS_BY_COURIER,
                    new Object[]{id, choicesSend[selectedPosition]}, new ResultListener() {
                        @Override
                        public void onSuccess(String s) {
                            status = choicesSend[selectedPosition];
                            statusTextView.setText(choices[selectedPosition]);
                            if (context != null) {
                                Toast.makeText(context, R.string.status_changed, Toast.LENGTH_SHORT).show();
                                //getActivity().finish();
                            }
                        }

                        @Override
                        public void onError(String error, String reason, String details) {
                            if (context != null)
                                Toast.makeText(context, R.string.check_internet_connection_hint, Toast.LENGTH_LONG).show();
                        }
                    });
        }
    }


    private void openCamera() {
        if (ContextCompat.checkSelfPermission(context,
                kz.clouddelivery.daas.Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED) {
            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(cameraIntent, CAMERA_REQUEST);
        } else {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{kz.clouddelivery.daas.Manifest.permission.CAMERA}, 0);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get(TAG_DATA);
            if (photo != null) {
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                photo.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                byte[] b = stream.toByteArray();
                String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
                String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
                uploadImageCall(encodedImage, currentDateTimeString);
                getImages();
            }
        }
    }

    private void getImages() {
        if (MeteorSingleton.hasInstance() && MeteorSingleton.getInstance().isConnected()) {
            MeteorSingleton.getInstance().call(ConstantsHolder.CALL_GET_IMAGE_URL_MOBILE,
                    new Object[]{id}, new ResultListener() {
                        @Override
                        public void onSuccess(String result) {
                            ArrayList<String> urls = new ArrayList<>();
                            String profileJson = "{" + TAG_DESCRIPTION + ":" + result + "}";
                            try {
                                JSONObject jsonObject = new JSONObject(profileJson);
                                JSONArray array = jsonObject.getJSONArray(TAG_DESCRIPTION);
                                for (int i = 0; i < array.length(); i++) {
                                    urls.add(array.get(i).toString());
                                }

                                galleryRecyclerView.setAdapter(new HorizontalAdapter(urls, getActivity()));
                                galleryRecyclerView.setLayoutManager(horizontalLayoutManager);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onError(String error, String reason, String details) {

                        }
                    });
        }
    }


    private void uploadImageCall(String image, String date) {
        if (MeteorSingleton.getInstance().isConnected()) {
            MeteorSingleton.getInstance().call(CALL_SEND_IMAGE,
                    new Object[]{id, date, image}, new ResultListener() {
                        @Override
                        public void onSuccess(String result) {
                            if (context != null)
                                Toast.makeText(context, R.string.image_uploaded, Toast.LENGTH_SHORT).show();
                            getOrderInfo(id);

                        }

                        @Override
                        public void onError(String error, String reason, String details) {
                            if (context != null)
                                Toast.makeText(context, R.string.uploading_error, Toast.LENGTH_SHORT).show();
                        }
                    });
        }
    }
}
