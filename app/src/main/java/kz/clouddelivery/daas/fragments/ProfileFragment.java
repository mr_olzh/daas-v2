package kz.clouddelivery.daas.fragments;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import im.delight.android.ddp.MeteorSingleton;
import im.delight.android.ddp.ResultListener;
import kz.clouddelivery.daas.R;
import kz.clouddelivery.daas.activities.LoginActivity;
import kz.clouddelivery.daas.holders.ConstantsHolder;
import kz.clouddelivery.daas.holders.InstanceHolder;

public class ProfileFragment extends BaseFragment implements View.OnClickListener {

    private static final String TAG_DESCRIPTION = "description";
    private static final String TAG_VALUE = "value";
    private static final String TAG_KEY = "key";
    private static final String TAG_FIO = "fio";
    private static final String TAG_PHONE = "phone";
    private static final String TAG_EMAIL = "email";
    private static final String TAG_CAR = "car";
    private static final String TAG_STATUS = "status";
    private static final String TAG_ONLINE = "online";
    private static final String TAG_MEALTIME = "mealtime";
    private static final String TAG_PROFILE_ACTIVITY = "Profile fragment";

    private ProgressBar progressBar;
    private TextView nameTextView, numberTextView, emailTextView, transportTextView, statusTextView;
    private String status;
    private String[] choices;
    private String[] choicesSend;
    private int selectedPosition = 0;
    private HashMap<String, Integer> statusHashMapOrder = new HashMap<>();

    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        nameTextView = (TextView) view.findViewById(R.id.nameTextView);
        numberTextView = (TextView) view.findViewById(R.id.numberTextView);
        emailTextView = (TextView) view.findViewById(R.id.emailTextView);
        transportTextView = (TextView) view.findViewById(R.id.transportTextView);

        //ratingBar = (RatingBar) view.findViewById(R.id.ratingBar);
        Button logoutButton = (Button) view.findViewById(R.id.logoutButton);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        statusTextView = (TextView) view.findViewById(R.id.statusTextView);
        ImageView changeImageView = (ImageView) view.findViewById(R.id.statusImageView);
        changeImageView.setOnClickListener(this);

        logoutButton.setOnClickListener(this);

        progressBar.setVisibility(View.VISIBLE);
        getAndSetProfileInfo();
        getStatusList();
        return view;
    }

    private void getAndSetProfileInfo() {
        if (!MeteorSingleton.hasInstance()) {
            InstanceHolder.createMeteorInstance(getContext());
        }
        if (MeteorSingleton.hasInstance() && MeteorSingleton.getInstance().isConnected()) {
            MeteorSingleton.getInstance().call(ConstantsHolder.CALL_GET_COURIER_PROFILE, new ResultListener() {

                @Override
                public void onSuccess(String result) {
                    String profileJson = "{" + TAG_DESCRIPTION + ":" + result + "}";
                    try {
                        JSONObject jsonObj = new JSONObject(profileJson);
                        JSONObject info = jsonObj.getJSONObject(TAG_DESCRIPTION);

                        String courierName = info.getString(TAG_FIO);
                        nameTextView.setText(courierName);
                        String courierPhone = info.getString(TAG_PHONE);
                        numberTextView.setText(courierPhone);
                        String courierEmail = info.getString(TAG_EMAIL);
                        emailTextView.setText(courierEmail);
                        String courierCar = info.getString(TAG_CAR);
                        transportTextView.setText(courierCar);
                        status = info.getString(TAG_STATUS);

                        setStatus(status);
                        progressBar.setVisibility(View.INVISIBLE);

                    } catch (JSONException | NullPointerException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(String error, String reason, String details) {
                    //Log.e(TAG_PROFILE_ACTIVITY, ConstantsHolder.TAG_METEOR_RESULT_ERROR);
                    Log.e("Meteor error", error + ":" + reason + ":" + details);
                }
            });
        }
    }

    @Override
    public void onResume() {
        getAndSetProfileInfo();
        super.onResume();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.logoutButton:
                showExitDialog(getContext(), getResources().getString(R.string.log_out),
                        getString(R.string.left_from_system));
                break;
            case R.id.statusImageView:
                if (choices != null) {
                    showStatusSelectionDialog(getContext(), getString(R.string.select));
                }
                break;
        }
    }

    private void showStatusSelectionDialog(Context context, String title) {
        AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);

        int item_number = 0;
        if (statusHashMapOrder.containsKey(status)) {
            item_number = statusHashMapOrder.get(status);
        }

        builder.setTitle(title)
                .setSingleChoiceItems(choices, item_number, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        selectedPosition = arg1;
                    }

                })
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialog, int id) {
                        changeCourierStatus();

                    }
                }).setCancelable(false).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }).show();
    }

    private void changeCourierStatus() {
        if (MeteorSingleton.getInstance().isConnected()) {

            MeteorSingleton.getInstance().call(ConstantsHolder.CALL_SET_COURIER_STATUS,
                    new Object[]{choicesSend[selectedPosition]}, new ResultListener() {
                        @Override
                        public void onSuccess(String s) {
                            //Toast.makeText(getActivity(), R.string.status_changed, Toast.LENGTH_SHORT).show();
                            status = choicesSend[selectedPosition];
                            Log.e("status", status);
                            setStatus(status);
                        }

                        @Override
                        public void onError(String error, String reason, String details) {
                            Toast.makeText(getActivity(), R.string.check_internet_connection_hint, Toast.LENGTH_LONG).show();
                        }
                    });
        } else {
            Toast.makeText(getActivity(), R.string.check_internet_connection_hint, Toast.LENGTH_LONG).show();
            //MeteorSingleton.getInstance().reconnect();
        }
    }

    private void setStatus(String status) {
        Log.e("status", status);
        switch (status) {
            case TAG_ONLINE:
                statusTextView.setText(R.string.online);
                break;
            case TAG_MEALTIME:
                statusTextView.setText(R.string.mealtime);
                break;
            default:
                statusTextView.setText(R.string.offline);
                break;
        }
    }


    public void showExitDialog(Context context, String title, String message) {
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setNeutralButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .setPositiveButton(R.string.exit, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        logOutCall();
                    }
                })
                .show();
    }

    public void getStatusList() {
        statusHashMapOrder.clear();

        if (MeteorSingleton.getInstance().isConnected()) {
            MeteorSingleton.getInstance().call(ConstantsHolder.CALL_GET_COURIER_STATUSES, new ResultListener() {

                @Override
                public void onSuccess(String result) {
                    String profileJson = "{" + TAG_DESCRIPTION + ":" + result + "}";
                    Log.i(ConstantsHolder.CALL_GET_COURIER_STATUSES, profileJson);
                    JSONObject jsonObj;
                    try {
                        jsonObj = new JSONObject(profileJson);
                        JSONArray array = jsonObj.getJSONArray(TAG_DESCRIPTION);
                        choices = new String[array.length()];
                        choicesSend = new String[array.length()];
                        for (int i = 0; i < array.length() && array.length() != 0; i++) {
                            JSONObject object = array.getJSONObject(i);
                            String statusName = object.getString(TAG_VALUE);
                            String statusKey = object.getString(TAG_KEY);
                            choices[i] = statusName;
                            choicesSend[i] = statusKey;
                            statusHashMapOrder.put(statusKey, i);
                        }
                    } catch (JSONException | NullPointerException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(String error, String reason, String details) {
                    Log.e(TAG_PROFILE_ACTIVITY, ConstantsHolder.TAG_METEOR_RESULT_ERROR);
                }
            });
        }
    }

    private void logOutCall() {
        if (MeteorSingleton.hasInstance() && MeteorSingleton.getInstance().isConnected()) {
            MeteorSingleton.getInstance().call(ConstantsHolder.CALL_LOG_OUT, new ResultListener() {

                @Override
                public void onSuccess(String result) {
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    MeteorSingleton.getInstance().logout();
                    cleanSharedPreferences(ConstantsHolder.TAG_LOGIN_AND_PASSWORD);
                    startActivity(intent);
                    InstanceHolder.getMeteorSingleton().removeCallbacks();
                    getActivity().finish();
                }

                @Override
                public void onError(String error, String reason, String details) {
                    //Log.e(TAG_MAIN_ACTIVITY, ConstantsHolder.TAG_METEOR_RESULT_ERROR);
                }
            });
        }
    }
}
