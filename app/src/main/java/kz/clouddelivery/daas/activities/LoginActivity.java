package kz.clouddelivery.daas.activities;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.crashlytics.android.Crashlytics;
import com.google.firebase.iid.FirebaseInstanceId;

import im.delight.android.ddp.MeteorSingleton;
import im.delight.android.ddp.ResultListener;
import kz.clouddelivery.daas.R;
import kz.clouddelivery.daas.holders.ConstantsHolder;
import kz.clouddelivery.daas.holders.FunctionsHolder;

public class LoginActivity extends BaseActivity implements View.OnClickListener {

    public static String token;
    Button loginButton;
    EditText emailEditTex, passwordEditText;
    TextInputLayout emailInputLayout, passwordInputLayout;
    String email, password;
    ProgressBar progressBar;
    private BroadcastReceiver mReceiver;
    private SharedPreferences sp;

    public static boolean isValidEmail(CharSequence target) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static boolean isEmpty(CharSequence target) {
        return TextUtils.isEmpty(target);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initToolbar(R.id.toolbar);
        setToolbarText(R.id.toolbar, getResources().getString(R.string.welcome));

        sp = PreferenceManager.getDefaultSharedPreferences(this);

        loginButton = (Button) findViewById(R.id.loginButton);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        loginButton.setOnClickListener(this);

        emailInputLayout = (TextInputLayout) findViewById(R.id.email_input_layout);
        emailEditTex = (EditText) findViewById(R.id.emailEditText);
        emailEditTex.setText(sp.getString(ConstantsHolder.TAG_LOGIN, ConstantsHolder.EMPTY_STRING));
        emailEditTex.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!isValidEmail(s)) {
                    emailInputLayout.setError(getString(R.string.Invalid_email));
                } else if (isEmpty(s)) {
                    emailInputLayout.setError(getString(R.string.Email_cannot_be_empty));
                } else {
                    emailInputLayout.setError(ConstantsHolder.EMPTY_STRING);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        passwordInputLayout = (TextInputLayout) findViewById(R.id.password_input_layout);
        passwordEditText = (EditText) findViewById(R.id.passEditText);
        passwordEditText.setText(sp.getString(ConstantsHolder.TAG_PASSWORD, ConstantsHolder.EMPTY_STRING));
        passwordEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (isEmpty(s)) {
                    passwordInputLayout.setError(getString(R.string.Enter_password_please));
                } else {
                    passwordInputLayout.setError(ConstantsHolder.EMPTY_STRING);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.loginButton) {
            new FunctionsHolder().hideKeyboard(this);
            email = emailEditTex.getText().toString();
            password = passwordEditText.getText().toString();
            if (!isEmpty(email) && !isEmpty(password) && isValidEmail(email)) {
                progressBar.setVisibility(View.VISIBLE);
                if (FunctionsHolder.isOnline(this)) {
                    signIn();
                } else {
                    FunctionsHolder.showAlertDialog(this, getResources().getString(R.string.error), getResources().getString(R.string.check_internet_connection_hint));
                }

            }

            if (isEmpty(email)) {
                emailInputLayout.setError(getString(R.string.Email_cannot_be_empty));
            } else if (!isValidEmail(email)) {
                emailInputLayout.setError(getString(R.string.Invalid_email));
            }

            if (isEmpty(password)) {
                passwordInputLayout.setError(getString(R.string.Enter_password_please));
            }

        }
    }

    private void signIn() {
        if (!MeteorSingleton.hasInstance()) {
            reConnectInstance();
        }
        if (!MeteorSingleton.getInstance().isConnected()) {
            MeteorSingleton.getInstance().connect();
        }
        if (MeteorSingleton.getInstance().isConnected()) {
            MeteorSingleton.getInstance().loginWithEmail(email, password, new ResultListener() {

                @Override
                public void onSuccess(String result) {
                    progressBar.setVisibility(View.INVISIBLE);
                    token = FirebaseInstanceId.getInstance().getToken();
                    Log.e("token", token + "");
                    MeteorSingleton.getInstance().call(ConstantsHolder.CALL_SET_ANDROID_REG_ID, new Object[]{token});
                    MeteorSingleton.getInstance().unsubscribe(FunctionsHolder.getApplicationsMobileSubscribeId(getApplicationContext()));
                    MeteorSingleton.getInstance().unsubscribe(FunctionsHolder.getStatusSubscribeId(getApplicationContext()));
                    String subscriptionId = MeteorSingleton.getInstance().subscribe(ConstantsHolder.SUBSCRIBE_APPLICATION,
                            new Object[]{30});
                    FunctionsHolder.saveApplicationsMobileSubscribeId(getApplicationContext(), subscriptionId);
                    String statusId = MeteorSingleton.getInstance().subscribe(ConstantsHolder.SUBSCRIBE_STATUSES);
                    FunctionsHolder.saveStatusSubscribeId(getApplicationContext(), statusId);
                    SharedPreferences.Editor editor = sp.edit();
                    editor.putString(ConstantsHolder.TAG_LOGIN, email);
                    editor.apply();

                    logUser(email, token);
                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
                    finish();

                }

                @Override
                public void onError(String error, String reason, String details) {
                    progressBar.setVisibility(View.INVISIBLE);
                    FunctionsHolder.showAlertDialog(LoginActivity.this, getString(R.string.error), getString(R.string.this_user_is_does_not_exist));
                }

            });
        } else {
            progressBar.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mReceiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mReceiver);
            mReceiver = null;
        }
    }

    private void logUser(String user, String token) {
        Crashlytics.setUserEmail(user);
        Crashlytics.setUserIdentifier(token);
        Crashlytics.setString("user", user + "");
    }
}
