package kz.clouddelivery.daas.activities;

import android.content.Context;
import android.content.ContextWrapper;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.squareup.otto.Subscribe;

import im.delight.android.ddp.MeteorSingleton;
import kz.clouddelivery.daas.R;
import kz.clouddelivery.daas.RelogBus;
import kz.clouddelivery.daas.RelogCallback;
import kz.clouddelivery.daas.custom.LocaleContextWrapper;
import kz.clouddelivery.daas.fragments.BaseFragment;
import kz.clouddelivery.daas.holders.ConstantsHolder;
import kz.clouddelivery.daas.holders.FunctionsHolder;
import kz.clouddelivery.daas.holders.InstanceHolder;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static kz.clouddelivery.daas.RelogCallback.METEOR_CREATED_BASE_ACTIVITY;
import static kz.clouddelivery.daas.holders.ConstantsHolder.EMPTY_STRING;
import static kz.clouddelivery.daas.holders.ConstantsHolder.LOCALE_RU;


public abstract class BaseActivity extends AppCompatActivity {

    @Nullable
    private Snackbar mNetworkConnectionSnackbar;
    private Object mConnectionCallback = new Object() {

        @Subscribe
        public void onConnectionStatusChanged(NetworkConnectionEvent event) {
            if (event.getStatus() == NetworkConnectionEvent.DISCONNECTED) {
                mNetworkConnectionSnackbar = Snackbar.make(getRootView(), R.string.internet_connection_hint, Snackbar.LENGTH_INDEFINITE);
                mNetworkConnectionSnackbar.show();
            } else if (mNetworkConnectionSnackbar != null && mNetworkConnectionSnackbar.isShown()) {
                mNetworkConnectionSnackbar.dismiss();
                reConnectInstance();
            }
        }
    };

    public void reConnectInstance() {
        MeteorSingleton meteorSingleton = InstanceHolder.createMeteorInstance(getApplicationContext());
        RelogCallback relogCallback = new RelogCallback(getApplicationContext(), null, METEOR_CREATED_BASE_ACTIVITY);
        String subscriptionId = MeteorSingleton.getInstance().subscribe(ConstantsHolder.SUBSCRIBE_APPLICATION,
                new Object[]{30});

        MeteorSingleton.getInstance().unsubscribe(FunctionsHolder.getApplicationsMobileSubscribeId(getApplicationContext()));
        FunctionsHolder.saveApplicationsMobileSubscribeId(getApplicationContext(), subscriptionId);

        MeteorSingleton.getInstance().unsubscribe(FunctionsHolder.getStatusSubscribeId(getApplicationContext()));
        String statusId = MeteorSingleton.getInstance().subscribe(ConstantsHolder.SUBSCRIBE_STATUSES);
        FunctionsHolder.saveStatusSubscribeId(getApplicationContext(), statusId);
        if (meteorSingleton != null) {
            meteorSingleton.removeCallbacks();
            meteorSingleton.addCallback(relogCallback);
            meteorSingleton.connect();
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        ContextWrapper contextWrapper = LocaleContextWrapper.wrap(newBase, LOCALE_RU);
        super.attachBaseContext(CalligraphyContextWrapper.wrap(contextWrapper));
    }

    protected void replaceFragment(@NonNull BaseFragment fragment, boolean isStacked) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.container, fragment);
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        if (isStacked) {
            transaction.addToBackStack(fragment.getClass().getSimpleName());
        }
        transaction.commit();
    }

    @Nullable
    protected ActionBar initToolbar(@IdRes int toolbarId) {
        Toolbar toolbar = (Toolbar) findViewById(toolbarId);
        setSupportActionBar(toolbar);
        return getSupportActionBar();
    }

    @Nullable
    protected ActionBar setToolbarTitle(String title) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(title);
        }
        return actionBar;
    }

    @Nullable
    protected Toolbar setToolbarText(@IdRes int toolbarId, String text) {
        Toolbar toolbar = (Toolbar) findViewById(toolbarId);
        setToolbarTitle(EMPTY_STRING);
        TextView toolbarTV = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbarTV.setText(text);
        return toolbar;
    }

    protected ActionBar setHomeButton() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        return actionBar;
    }

    private View getRootView() {
        return findViewById(R.id.content);
    }

    @Override
    protected void onResume() {
        super.onResume();
        RelogBus.getInstance().register(mConnectionCallback);
    }

    @Override
    protected void onPause() {
        super.onPause();
        RelogBus.getInstance().unregister(mConnectionCallback);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        new FunctionsHolder().hideKeyboard(this);
    }


    public static class NetworkConnectionEvent {
        public static final int CONNECTED = 0;
        public static final int DISCONNECTED = 1;

        private final int mStatus;

        public NetworkConnectionEvent(int status) {
            mStatus = status;
        }

        public int getStatus() {
            return mStatus;
        }
    }

}