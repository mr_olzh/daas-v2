package kz.clouddelivery.daas.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.holder.BadgeStyle;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

import im.delight.android.ddp.MeteorSingleton;
import im.delight.android.ddp.ResultListener;
import kz.clouddelivery.daas.R;
import kz.clouddelivery.daas.custom.MapLocationListener;
import kz.clouddelivery.daas.fragments.OrderTabsFragment;
import kz.clouddelivery.daas.fragments.ProfileFragment;
import kz.clouddelivery.daas.fragments.RoutesFragment;
import kz.clouddelivery.daas.holders.ConstantsHolder;
import kz.clouddelivery.daas.holders.FunctionsHolder;
import kz.clouddelivery.daas.holders.InstanceHolder;


public class MainActivity extends BaseActivity implements Drawer.OnDrawerItemClickListener {

    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2;
    private static final String TAG_DESCRIPTION = "description";
    private static final String TAG_FIO = "fio";
    private static final String TAG_EMAIL = "email";
    private static final String TAG_MAIN_ACTIVITY = "Main Activity";
    private static final String TAG_ID = "id";
    private static final String TAG_TYPE = "type";
    @SuppressLint("StaticFieldLeak")
    public static MapLocationListener mapListener;
    static int selection = 0;
    @SuppressLint("StaticFieldLeak")
    private static Drawer result;
    private static Timer timer;
    private AccountHeader headerResult;
    private Boolean isConnected = true;

    public static void updateBadge(Context context, String count) {
        BadgeStyle whiteOnPrimaryBadgeStyle = new BadgeStyle().withTextColor(Color.WHITE)
                .withColorRes(R.color.primary);
        PrimaryDrawerItem ordersItem = createDrawerItem(context, 1, R.string.orders, count, R.drawable.orders,
                true, R.color.primary, true, whiteOnPrimaryBadgeStyle);

        if (selection == 1) {
            ordersItem.withSetSelected(true);
        }
        //Log.e("selection", selection + "");
        result.updateItem(ordersItem);
    }

    @NonNull
    private static PrimaryDrawerItem createDrawerItem(Context context, long id, @StringRes int name, @Nullable String badge,
                                                      @DrawableRes int icon, boolean isIconTintEnabled,
                                                      @ColorRes int selectedIconColor, boolean isEnabled,
                                                      @Nullable BadgeStyle badgeStyle) {

        PrimaryDrawerItem primaryDrawerItem = new PrimaryDrawerItem().withIdentifier(id).withName(name)
                .withIcon(icon);
        if (badge != null) {
            primaryDrawerItem.withBadge(badge);
        }
        if (isEnabled) {
            primaryDrawerItem.withEnabled(true);
        }
        primaryDrawerItem.withSelectedIconColor(ContextCompat.getColor(context, selectedIconColor))
                .withIconTintingEnabled(isIconTintEnabled);

        if (badgeStyle != null) {
            primaryDrawerItem.withBadgeStyle(badgeStyle);
        }

        return primaryDrawerItem;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initToolbar(R.id.toolbar);


        Toolbar toolbar = setToolbarText(R.id.toolbar, getResources().getString(R.string.orders));
        setHomeButton();

        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();
        }

        OrderTabsFragment orderTabsFragment = new OrderTabsFragment();
        replaceFragment(orderTabsFragment, false);

        Intent oldIntent = getIntent();
        String id = oldIntent.getStringExtra(TAG_ID);
        String type = oldIntent.getStringExtra(TAG_TYPE);

        //Log.e("data", id + ":" + type);

        if (id != null && type != null) {
            switch (type) {
                case "chat":
                    startActivity(OrderActivity.createIntent(getApplicationContext(), id, true));
                    break;
                case "app":
                    startActivity(OrderActivity.createIntent(getApplicationContext(), id, false));
                    break;
            }
        }

        if (FunctionsHolder.isOnline(this)) {
            getProfileInfo();
        }

        mapListener = new MapLocationListener(this);
        buildGoogleApiClient();

        headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.drawable.header)
                .withOnAccountHeaderListener(new AccountHeader.OnAccountHeaderListener() {
                    @Override
                    public boolean onProfileChanged(View view, IProfile profile, boolean currentProfile) {
                        return false;
                    }
                })
                .withOnAccountHeaderProfileImageListener(new AccountHeader.OnAccountHeaderProfileImageListener() {
                    @Override
                    public boolean onProfileImageClick(View view, IProfile profile, boolean current) {
                        ProfileFragment profileFragment = new ProfileFragment();
                        replaceFragment(profileFragment, false);
                        setToolbarText(R.id.toolbar, getResources().getString(R.string.profile));
                        return false;
                    }

                    @Override
                    public boolean onProfileImageLongClick(View view, IProfile profile, boolean current) {
                        ProfileFragment profileFragment = new ProfileFragment();
                        replaceFragment(profileFragment, false);
                        setToolbarText(R.id.toolbar, getResources().getString(R.string.profile));
                        return false;
                    }
                })
                .build();

        BadgeStyle whiteOnPrimaryBadgeStyle = new BadgeStyle().withTextColor(Color.WHITE)
                .withColorRes(R.color.primary);
        PrimaryDrawerItem ordersItem = createDrawerItem(getApplicationContext(), 1, R.string.orders, "0", R.drawable.orders,
                true, R.color.primary, true, whiteOnPrimaryBadgeStyle).withSetSelected(true);
        PrimaryDrawerItem routesItem = createDrawerItem(getApplicationContext(), 2, R.string.routes, null, R.drawable.routes,
                true, R.color.primary, true, null);
        PrimaryDrawerItem profileItem = createDrawerItem(getApplicationContext(), 3, R.string.profile, null, R.drawable.profile,
                true, R.color.primary, true, null);
        PrimaryDrawerItem exitItem = createDrawerItem(getApplicationContext(), 4, R.string.exit, null, R.drawable.exit,
                true, R.color.primary, true, null);

        if (toolbar != null) {
            result = new DrawerBuilder()
                    .withActivity(this)
                    .withToolbar(toolbar)
                    .withAccountHeader(headerResult)
                    .addDrawerItems(
                            ordersItem,
                            routesItem,
                            profileItem,
                            new DividerDrawerItem(),
                            exitItem
                    )
                    .withOnDrawerItemClickListener(this)
                    .build();
        }
        getAndSetVersionOfApplication();
    }

    private void getAndSetVersionOfApplication() {
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String version = null;
        if (pInfo != null) {
            version = pInfo.versionName;
        }
        result.addStickyFooterItem(new SecondaryDrawerItem().withName(getString(R.string.version) + " " + version + "   " + getString(R.string.company_domain))
                .withSelectable(false).withTextColor(ContextCompat.getColor(this, R.color.versionColor))
                .withEnabled(false)
        );
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.enable_geolocation_question)
                .setCancelable(false)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }


    @Override
    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
        switch ((int) drawerItem.getIdentifier()) {
            case 1:
                OrderTabsFragment orderTabsFragment = new OrderTabsFragment();
                replaceFragment(orderTabsFragment, false);
                setToolbarText(R.id.toolbar, getResources().getString(R.string.orders));
                selection = 1;
                break;
            case 2:
                RoutesFragment routesFragment = new RoutesFragment();
                replaceFragment(routesFragment, false);
                setToolbarText(R.id.toolbar, getResources().getString(R.string.routes));
                selection = 2;
                break;
            case 3:
                ProfileFragment profileFragment = new ProfileFragment();
                replaceFragment(profileFragment, false);
                setToolbarText(R.id.toolbar, getResources().getString(R.string.profile));
                selection = 3;
                break;
            case 4:
                InstanceHolder.getMeteorSingleton().removeCallbacks();
                finishAffinity();
                break;
            default:
                break;
        }

        return false;
    }

    private void getProfileInfo() {
        if (!MeteorSingleton.hasInstance()) {
            InstanceHolder.createMeteorInstance(this);
        }
        if (MeteorSingleton.hasInstance() && MeteorSingleton.getInstance().isConnected()) {
            MeteorSingleton.getInstance().call(ConstantsHolder.CALL_GET_COURIER_PROFILE, new ResultListener() {

                @Override
                public void onSuccess(String result) {
                    String profileJson = "{" + TAG_DESCRIPTION + ":" + result + "}";
                    try {
                        JSONObject jsonObj = new JSONObject(profileJson);
                        JSONObject info = jsonObj.getJSONObject(TAG_DESCRIPTION);

                        String courierName = info.getString(TAG_FIO);
                        String courierEmail = info.getString(TAG_EMAIL);

                        ProfileDrawerItem mainProfile = new ProfileDrawerItem().withName(courierName).withEmail(courierEmail);
                        headerResult.addProfile(mainProfile, 0);

                    } catch (JSONException | NullPointerException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(String error, String reason, String details) {
                    Log.e(TAG_MAIN_ACTIVITY, ConstantsHolder.TAG_METEOR_RESULT_ERROR);
                }
            });
        }
    }

    @Override
    protected void onStart() {
        startTimer();
        if (MapLocationListener.mGoogleApiClient != null) {
            MapLocationListener.mGoogleApiClient.connect();
        }
        if (MeteorSingleton.hasInstance() && !MeteorSingleton.getInstance().isConnected()
                && !MeteorSingleton.getInstance().isLoggedIn()) {
            FunctionsHolder.restartApplication(this);
        }
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        stopLocationUpdates();
        stopTimer();
        InstanceHolder.disconnectMeteor();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (result.isDrawerOpen()) {
            result.closeDrawer();
        } else {
            super.onBackPressed();
        }
    }


    @Override
    protected void onResume() {
        if (!MeteorSingleton.hasInstance()) {
            reConnectInstance();
        }
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    protected synchronized void buildGoogleApiClient() {
        Log.e(TAG_MAIN_ACTIVITY, "Building GoogleApiClient");
        MapLocationListener.mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(mapListener)
                .addOnConnectionFailedListener(mapListener)
                .addApi(LocationServices.API)
                .build();
        createLocationRequest();
    }

    protected void createLocationRequest() {
        MapLocationListener.mLocationRequest = new LocationRequest();
        MapLocationListener.mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        MapLocationListener.mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        MapLocationListener.mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    protected void stopLocationUpdates() {
        Log.e("LOCATION", "STOP UPDATES");
        if (MapLocationListener.mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(MapLocationListener.mGoogleApiClient, mapListener);
        }
    }

    private void startTimer() {
        if (timer != null) {
            return;
        }
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        isConnected = false;
                        startHandler();
                        //Log.e("time", isConnected + "");
                        if (MeteorSingleton.hasInstance() && MeteorSingleton.getInstance().isConnected()) {
                            MeteorSingleton.getInstance().call(ConstantsHolder.CALL_CHECK_STATUS_CONNECTION,
                                    new ResultListener() {
                                        @Override
                                        public void onSuccess(String result) {
                                            isConnected = true;
                                            Log.e("time", isConnected + "");
                                        }

                                        @Override
                                        public void onError(String error, String reason, String details) {
                                            isConnected = true;
                                            Log.e("time", isConnected + "");
                                        }
                                    });
                        }
                    }
                });
            }
        }, 0, 1000 * 60);
    }

    public void stopTimer() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    public void startHandler() {
        Handler handler = new Handler();

        handler.postDelayed(new Runnable() {
            public void run() {
                Log.e("timeHandler", isConnected + "");
                if (!isConnected) {
                    reConnectInstance();
                }
            }
        }, 5000);
    }
}
