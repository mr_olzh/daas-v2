package kz.clouddelivery.daas.activities;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.Menu;
import android.view.MenuItem;

import kz.clouddelivery.daas.Manifest;
import kz.clouddelivery.daas.R;
import kz.clouddelivery.daas.fragments.ChatFragment;
import kz.clouddelivery.daas.fragments.OrderDetailsFragment;
import  im.delight.android.ddp.MeteorSingleton;
import kz.clouddelivery.daas.holders.FunctionsHolder;

public class OrderActivity extends BaseActivity {

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    private static final String ID_KEY = "id";
    private static final String SHOULD_OPEN_CHAT = "should_open_chat";

    String[] permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.CALL_PHONE};

    public static Intent createIntent(Context context, String documentId,
                                      boolean shouldOpenChat) {

        Intent intent = new Intent(context, OrderActivity.class);
        intent.putExtra(ID_KEY, documentId);
        intent.putExtra(SHOULD_OPEN_CHAT, shouldOpenChat);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        initToolbar(R.id.toolbar);
        setHomeButton();



        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
            hasPermissionInManifest();
        }

        if (savedInstanceState == null) {
            String documentId = getIntent().getStringExtra(ID_KEY);

            boolean shouldOpenChat = getIntent().getBooleanExtra(SHOULD_OPEN_CHAT, false);
            OrderDetailsFragment orderDetailsFragment = OrderDetailsFragment.newInstance(documentId);
            Bundle bundle = new Bundle();
            bundle.putString(ID_KEY, documentId);
            orderDetailsFragment.setArguments(bundle);
            replaceFragment(orderDetailsFragment, false);
            if (shouldOpenChat) {
                ChatFragment chatFragment = ChatFragment.newInstance(documentId);
                chatFragment.setArguments(bundle);
                replaceFragment(chatFragment, true);
            }
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.C2D_MESSAGE},
                        MY_PERMISSIONS_REQUEST_LOCATION);

            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    public void hasPermissionInManifest() {
        for (String permission : permissions) {
            if (ContextCompat.checkSelfPermission(this,
                    permission)
                    != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        permission)) {
                } else {
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                    Manifest.permission.CAMERA, Manifest.permission.CALL_PHONE},
                            MY_PERMISSIONS_REQUEST_LOCATION);

                }
            }
        }
    }

    @Override
    protected void onResume() {
        if (!MeteorSingleton.hasInstance()) {
            reConnectInstance();
        }
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (MeteorSingleton.hasInstance() && !MeteorSingleton.getInstance().isConnected()
                && !MeteorSingleton.getInstance().isLoggedIn()) {
            FunctionsHolder.restartApplication(this);
        }
    }
}
