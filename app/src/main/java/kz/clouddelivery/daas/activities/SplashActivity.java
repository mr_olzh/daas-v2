package kz.clouddelivery.daas.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import org.jsoup.Jsoup;

import im.delight.android.ddp.MeteorSingleton;
import kz.clouddelivery.daas.R;
import kz.clouddelivery.daas.RelogCallback;
import kz.clouddelivery.daas.holders.FunctionsHolder;
import kz.clouddelivery.daas.holders.InstanceHolder;

import static kz.clouddelivery.daas.RelogCallback.METEOR_CREATED_SPLASH;

public class SplashActivity extends BaseActivity {

    private String currentVersion;
    private Boolean isActive = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        ActionBar actionBar = initToolbar(R.id.toolbar);
        if (actionBar != null) {
            actionBar.hide();
        }
        isActive = true;
        try {
            currentVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        if (FunctionsHolder.isOnline(this))
            new getVersionCode().execute();
        else
            FunctionsHolder.showAlertDialogAndFinish(this, this, getResources().getString(R.string.error),
                    getResources().getString(R.string.check_internet_connection_hint));
    }

    private class getVersionCode extends AsyncTask<Void, String, String> {
        @Override
        protected String doInBackground(Void... voids) {

            String newVersion;
            try {
                newVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=" + getPackageName() + "&hl=it")
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get()
                        .select("div[itemprop=softwareVersion]")
                        .first()
                        .ownText();
                return newVersion;
            } catch (Exception e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(String onlineVersion) {
            super.onPostExecute(onlineVersion);
            if (onlineVersion != null && !onlineVersion.isEmpty()) {
                if (Float.valueOf(currentVersion) < Float.valueOf(onlineVersion)) {
                    AlertDialog.Builder dialog = new AlertDialog.Builder(SplashActivity.this)
                            .setTitle("Обновление")
                            .setCancelable(false)
                            .setMessage("Доступна новая версия приложения, обновитесь")
                            .setNeutralButton("Обновить", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                    openApplicationFromMarket();
                                    SplashActivity.this.finish();
                                }
                            });
                    if (isActive && getApplicationContext() != null)
                        dialog.show();
                } else {
                    createMeteorInstance();
                }
            }
            //Log.e("update", "Current version " + currentVersion + " playstore version " + onlineVersion);
        }
    }

    private void openApplicationFromMarket() {
        final String appPackageName = getPackageName();
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    private void createMeteorInstance() {
        if (FunctionsHolder.isOnline(this)) {
            if (MeteorSingleton.hasInstance())
                MeteorSingleton.destroyInstance();
            MeteorSingleton meteorSingleton = InstanceHolder.createMeteorInstance(this);
            Log.e("Meteor created meteorSi", "SplashActivity new ");
            if (meteorSingleton != null) {
                RelogCallback relogCallback = new RelogCallback(this, getIntent(), METEOR_CREATED_SPLASH);
                meteorSingleton.removeCallbacks();
                meteorSingleton.addCallback(relogCallback);
            }
        } else {
            FunctionsHolder.showAlertDialogAndFinish(this, this, getResources().getString(R.string.error),
                    getResources().getString(R.string.check_internet_connection_hint));
        }
    }

    @Override
    protected void onDestroy() {
        isActive = false;
        super.onDestroy();
    }
}
