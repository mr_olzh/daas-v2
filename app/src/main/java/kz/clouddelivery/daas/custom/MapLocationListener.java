package kz.clouddelivery.daas.custom;

import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;

import im.delight.android.ddp.MeteorSingleton;
import im.delight.android.ddp.ResultListener;
import kz.clouddelivery.daas.holders.ConstantsHolder;
import kz.clouddelivery.daas.holders.LatLngHolder;

public class MapLocationListener implements LocationListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {
    public static GoogleApiClient mGoogleApiClient;
    public static LocationRequest mLocationRequest;
    public static LatLng lastPosition;
    private static Context context;
    private volatile Location mCurrentLocation;
    private static volatile Location mLastLocation;

    public MapLocationListener(Context appContext) {
        context = appContext;
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            mCurrentLocation = location;
            //Log.e("LOCATION", "CHANGED");
            sendPosition();
            lastPosition = new LatLng(location.getLatitude(), location.getLongitude());
        }
    }

    private void sendPosition() {
        if (MeteorSingleton.hasInstance()) {
            if (MeteorSingleton.getInstance().isConnected()) {
                if (mLastLocation == null && mCurrentLocation != null) {
                    mLastLocation = mCurrentLocation;
                    sendPositionCall(mCurrentLocation);
                } else {
                    LatLng currentPosition = new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
                    LatLng lastPosition = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                    if (LatLngHolder.shouldSendPosition(currentPosition, lastPosition)) {
                        mLastLocation = mCurrentLocation;
                        sendPositionCall(mCurrentLocation);
                    }
                }
            }
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.e("Service", "Connected to GoogleApiClient");
        if (ActivityCompat.checkSelfPermission(context,
                android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(context,
                        android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mLastLocation = null;
        if (mCurrentLocation == null) {
            mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            startLocationUpdates();
        }
    }

    private void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(context,
                android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(context,
                        android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        Log.e("LOCATION", "START UPDATES");
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    //Calls

    private void sendPositionCall(Location currentLocation) {
        if (MeteorSingleton.hasInstance() && MeteorSingleton.getInstance().isConnected()) {
            MeteorSingleton.getInstance().call(ConstantsHolder.CALL_SEND_COURIER_POSITION,
                    new Object[]{new double[][]{new double[]{currentLocation.getLatitude(),
                            currentLocation.getLongitude()}}}, new ResultListener() {
                        @Override
                        public void onSuccess(String result) {

                        }

                        @Override
                        public void onError(String error, String reason, String details) {

                        }
                    });
        }
    }
}
