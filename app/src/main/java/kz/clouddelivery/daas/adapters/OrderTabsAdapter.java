package kz.clouddelivery.daas.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import kz.clouddelivery.daas.R;
import kz.clouddelivery.daas.fragments.ActiveOrderFragment;
import kz.clouddelivery.daas.fragments.FinishedOrderFragment;


public class OrderTabsAdapter extends FragmentPagerAdapter {

    private Context context;

    public OrderTabsAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new ActiveOrderFragment();
            case 1:
                return new FinishedOrderFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        switch (position) {
            case 0:
                return context.getString(R.string.active);
            case 1:
                return context.getString(R.string.finish);
        }
        return null;
    }
}
