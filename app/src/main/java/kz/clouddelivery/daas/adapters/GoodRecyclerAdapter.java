package kz.clouddelivery.daas.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import kz.clouddelivery.daas.R;
import kz.clouddelivery.daas.models.Good;


public class GoodRecyclerAdapter extends RecyclerView.Adapter<GoodRecyclerAdapter.ViewHolder> {

    private List<Good> mItems;

    public GoodRecyclerAdapter(List<Good> mItems) {
        this.mItems = mItems;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.goods_list_item, viewGroup, false);

        return new ViewHolder(v);
    }


    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        Good item = mItems.get(i);
        viewHolder.idTextView.setText(item.getId());
        viewHolder.nameTextView.setText(item.getName());
        viewHolder.priceTextView.setText(item.getPrice());
        viewHolder.weightTextView.setText(item.getWeight());
        viewHolder.volumeTextView.setText(item.getVolume());
        viewHolder.amountTextView.setText(item.getAmount());
        viewHolder.totalPrice.setText(item.getTotal_price());
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView nameTextView, weightTextView, idTextView, priceTextView, volumeTextView, amountTextView, totalPrice;

        ViewHolder(View view) {
            super(view);
            nameTextView = (TextView) view.findViewById(R.id.name);
            weightTextView = (TextView) view.findViewById(R.id.weight);
            idTextView = (TextView) view.findViewById(R.id.code);
            priceTextView = (TextView) view.findViewById(R.id.price);
            volumeTextView = (TextView) view.findViewById(R.id.volume);
            amountTextView = (TextView) view.findViewById(R.id.count);
            totalPrice = (TextView)view.findViewById(R.id.totalPrice);
        }
    }
}
