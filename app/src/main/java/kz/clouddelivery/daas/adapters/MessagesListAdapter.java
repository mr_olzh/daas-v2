package kz.clouddelivery.daas.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import kz.clouddelivery.daas.R;
import kz.clouddelivery.daas.models.Comment;

public class MessagesListAdapter extends BaseAdapter {

    private Context context;
    private List<Comment> messagesItems;

    public MessagesListAdapter(Context context, List<Comment> navDrawerItems) {
        this.context = context;
        this.messagesItems = navDrawerItems;
    }

    @Override
    public int getCount() {
        return messagesItems.size();
    }

    @Override
    public Object getItem(int position) {
        return messagesItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Comment m = messagesItems.get(position);

        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (messagesItems.get(position).isSelf()) {
            convertView = mInflater.inflate(R.layout.message_right,
                    null);
        } else {
            convertView = mInflater.inflate(R.layout.message_left,
                    null);
        }

        TextView lblFrom = (TextView) convertView.findViewById(R.id.sender);
        TextView txtMsg = (TextView) convertView.findViewById(R.id.text);

        txtMsg.setText(m.getMessage());
        lblFrom.setText(m.getFromName());

        return convertView;
    }
}
