package kz.clouddelivery.daas.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.LinkedHashMap;
import java.util.List;

import im.delight.android.ddp.MeteorSingleton;
import im.delight.android.ddp.db.Document;
import kz.clouddelivery.daas.R;
import kz.clouddelivery.daas.activities.OrderActivity;
import kz.clouddelivery.daas.holders.ConstantsHolder;
import kz.clouddelivery.daas.models.Order;


public class OrderRecyclerAdapter extends RecyclerView.Adapter<OrderRecyclerAdapter.ViewHolder> {

    private static final String TAG_ID = "id";
    private List<Order> mItems;

    public OrderRecyclerAdapter(List<Order> mItems) {
        this.mItems = mItems;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.orders_list_item, viewGroup, false);

        return new ViewHolder(v);
    }


    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        Order item = mItems.get(i);
        viewHolder.numberTextView.setText(item.getCustomId());
        viewHolder.addressTextView.setText(item.getAddressTo());
        viewHolder.priceTextView.setText(item.getGeneralPrice());
        viewHolder.timeTextView.setText(item.getPlanDeliverPeriod());
        viewHolder.statusTextView.setText(item.getStatus());
        viewHolder.client_id = item.getClientId();
        viewHolder.id = mItems.get(i).getId();
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView numberTextView, addressTextView, priceTextView, timeTextView, statusTextView;
        ImageView callImageView;
        String id, client_id;

        ViewHolder(View view) {
            super(view);
            numberTextView = (TextView) view.findViewById(R.id.numberTextView);
            addressTextView = (TextView) view.findViewById(R.id.addressTextView);
            priceTextView = (TextView) view.findViewById(R.id.priceTextView);
            timeTextView = (TextView) view.findViewById(R.id.dateAndTimeTextView);
            statusTextView = (TextView) view.findViewById(R.id.statusTextView);
            callImageView = (ImageView) view.findViewById(R.id.callImageView);
            callImageView.setOnClickListener(this);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            try {
                Context context = view.getContext();
                if (view.getId() == R.id.callImageView) {
                    Document document = MeteorSingleton.getInstance().getDatabase().getCollection(ConstantsHolder.COLLECTION_APPLICATIONS)
                            .getDocument(id);
                    LinkedHashMap<String, Object> client = (LinkedHashMap<String, Object>)
                            document.getField("client");

                    Object number = client.get("phone1");
                    if (number != null) {
                        Intent callIntent = new Intent(Intent.ACTION_DIAL);
                        callIntent.setData(Uri.parse("tel:" + number.toString()));
                        context.startActivity(callIntent);
                    }
                } else {
                    Intent intent = new Intent(context, OrderActivity.class);
                    intent.putExtra(TAG_ID, id);
                    context.startActivity(intent);
                }
            } catch (Exception ignored) {

            }
        }
    }

}
