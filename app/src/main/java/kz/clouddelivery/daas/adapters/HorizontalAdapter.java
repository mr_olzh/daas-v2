package kz.clouddelivery.daas.adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import kz.clouddelivery.daas.R;
import kz.clouddelivery.daas.holders.ConstantsHolder;


public class HorizontalAdapter extends RecyclerView.Adapter<HorizontalAdapter.MyViewHolder> {

    private Context context;
    private List<String> horizontalList;

    public HorizontalAdapter(List<String> horizontalList, Context context) {
        this.horizontalList = horizontalList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.horizontal_recycler_view_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        Picasso
                .with(context)
                .load("https://" + ConstantsHolder.DEFAULT_IP + horizontalList.get(position))
                .resize(150, 150)
                .centerCrop()
                .into(holder.imageView);


        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(this,holder.txtView.getText().toString(),Toast.LENGTH_SHORT).show();
                showImage(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return horizontalList.size();
    }

    private void showImage(int i) {
        Dialog builder = new Dialog(context);
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {

            }
        });

        ImageView imageView = new ImageView(context);
        Picasso
                .with(context)
                .load("https://" + ConstantsHolder.DEFAULT_IP + horizontalList.get(i))
                .resize(600, 1000)
                .into(imageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        Log.e("image", "loaded");
                    }

                    @Override
                    public void onError() {
                        Log.e("image", "not loaded");
                    }
                });

        builder.addContentView(imageView, new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        builder.show();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;

        MyViewHolder(View view) {
            super(view);
            imageView = (ImageView) view.findViewById(R.id.image);

        }
    }
}
