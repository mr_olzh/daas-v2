package kz.clouddelivery.daas.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.LinkedHashMap;
import java.util.List;

import im.delight.android.ddp.MeteorSingleton;
import im.delight.android.ddp.db.Document;
import kz.clouddelivery.daas.R;
import kz.clouddelivery.daas.activities.OrderActivity;
import kz.clouddelivery.daas.holders.ConstantsHolder;
import kz.clouddelivery.daas.models.Order;


public class OrderPaginationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int ITEM = 0;
    private static final int LOADING = 1;
    private static final String TAG_ID = "id";

    private List<Order> orders;
    private Context context;

    private boolean isLoadingAdded = false;


    public OrderPaginationAdapter(Context context, List<Order> orders) {
        this.context = context;
        this.orders = orders;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case ITEM:
                viewHolder = getViewHolder(parent, inflater);
                break;
            case LOADING:
                View v2 = inflater.inflate(R.layout.item_progress, parent, false);
                viewHolder = new LoadingVH(v2);
                break;
        }
        return viewHolder;
    }

    @NonNull
    private RecyclerView.ViewHolder getViewHolder(ViewGroup parent, LayoutInflater inflater) {
        RecyclerView.ViewHolder viewHolder;
        View v1 = inflater.inflate(R.layout.orders_list_item, parent, false);
        viewHolder = new OrderVH(v1);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int i) {
        switch (getItemViewType(i)) {
            case ITEM:
                OrderVH viewHolder = (OrderVH) holder;
                Order item = orders.get(i);
                viewHolder.numberTextView.setText(item.getCustomId());
                viewHolder.addressTextView.setText(item.getAddressTo());
                viewHolder.priceTextView.setText(item.getGeneralPrice());
                viewHolder.timeTextView.setText(item.getPlanDeliverPeriod());
                viewHolder.statusTextView.setText(item.getStatus());
                viewHolder.client_id = item.getClientId();
                viewHolder.id = item.getId();
                break;
            case LOADING:
//                Do nothing
                break;
        }

    }

    @Override
    public int getItemCount() {
        return orders == null ? 0 : orders.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == orders.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }

    /*
   Helpers
   _________________________________________________________________________________________________
    */

    public void add(Order mc) {
        orders.add(mc);
        notifyItemInserted(orders.size() - 1);
    }

    public void addAll(List<Order> mcList) {
        for (Order mc : mcList) {
            add(mc);
        }
    }

    public void remove(Order city) {
        int position = orders.indexOf(city);
        if (position > -1) {
            orders.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        isLoadingAdded = false;
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }


    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new Order());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = orders.size() - 1;
        Order item = getItem(position);

        if (item != null) {
            orders.remove(position);
            notifyItemRemoved(position);
        }
    }

    private Order getItem(int position) {
        return orders.get(position);
    }


    private class OrderVH extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView numberTextView, addressTextView, priceTextView, timeTextView, statusTextView;
        ImageView callImageView;
        String id, client_id;

        OrderVH(View view) {
            super(view);
            numberTextView = (TextView) view.findViewById(R.id.numberTextView);
            addressTextView = (TextView) view.findViewById(R.id.addressTextView);
            priceTextView = (TextView) view.findViewById(R.id.priceTextView);
            timeTextView = (TextView) view.findViewById(R.id.dateAndTimeTextView);
            statusTextView = (TextView) view.findViewById(R.id.statusTextView);
            callImageView = (ImageView) view.findViewById(R.id.callImageView);
            callImageView.setOnClickListener(this);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Context context = view.getContext();
            if (view.getId() == R.id.callImageView) {
                Document document = MeteorSingleton.getInstance().getDatabase().getCollection(ConstantsHolder.COLLECTION_APPLICATIONS)
                        .getDocument(id);
                LinkedHashMap<String, Object> client = (LinkedHashMap<String, Object>)
                        document.getField("client");

                Object number = client.get("phone1");
                if (number != null) {
                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                    callIntent.setData(Uri.parse("tel:" + number.toString()));
                    context.startActivity(callIntent);
                }
            } else {
                Intent intent = new Intent(context, OrderActivity.class);
                intent.putExtra(TAG_ID, id);
                Log.e("id", id);
                context.startActivity(intent);
            }
        }
    }


    private class LoadingVH extends RecyclerView.ViewHolder {

        LoadingVH(View itemView) {
            super(itemView);
        }
    }
}
