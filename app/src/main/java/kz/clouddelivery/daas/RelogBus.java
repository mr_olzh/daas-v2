package kz.clouddelivery.daas;

import com.squareup.otto.Bus;

public class RelogBus extends Bus {

    private static Bus sBus;

    public static Bus getInstance() {
        if (sBus == null) {
            sBus = new Bus();
        }
        return sBus;
    }
}
