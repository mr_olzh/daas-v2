package kz.clouddelivery.daas;


import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.Log;

import im.delight.android.ddp.MeteorCallback;
import im.delight.android.ddp.MeteorSingleton;
import kz.clouddelivery.daas.activities.LoginActivity;
import kz.clouddelivery.daas.activities.MainActivity;
import kz.clouddelivery.daas.fragments.ActiveOrderFragment;
import kz.clouddelivery.daas.fragments.ChatFragment;
import kz.clouddelivery.daas.fragments.FinishedOrderFragment;
import kz.clouddelivery.daas.holders.ConstantsHolder;
import kz.clouddelivery.daas.holders.FunctionsHolder;

public class RelogCallback implements MeteorCallback {

    private static final String TAG_ID = "id";
    private static final String TAG_TYPE = "type";

    public static final int METEOR_CREATED_SPLASH = 0;
    public static final int METEOR_CREATED_BASE_ACTIVITY = 2;

    private Context context;
    private Intent intent;
    private int place;

    public RelogCallback(Context context, @Nullable Intent intent, int place) {
        this.context = context;
        this.intent = intent;
        this.place = place;
    }

    @Override
    public void onConnect(boolean signedInAutomatically) {
        switch (place) {
            case METEOR_CREATED_SPLASH:
                if (MeteorSingleton.getInstance().isLoggedIn() && context != null) {
                    MeteorSingleton.getInstance().unsubscribe(FunctionsHolder.getApplicationsMobileSubscribeId(context));
                    MeteorSingleton.getInstance().unsubscribe(FunctionsHolder.getStatusSubscribeId(context));
                    String subscriptionId = MeteorSingleton.getInstance().subscribe(ConstantsHolder.SUBSCRIBE_APPLICATION,
                            new Object[]{30});
                    FunctionsHolder.saveApplicationsMobileSubscribeId(context, subscriptionId);
                    String statusId = MeteorSingleton.getInstance().subscribe(ConstantsHolder.SUBSCRIBE_STATUSES);
                    FunctionsHolder.saveStatusSubscribeId(context, statusId);
                    Log.e("Meteor created", place + "/ logged in");
                    Intent new_intent = new Intent(context, MainActivity.class);
                    new_intent.putExtra(TAG_ID, intent.getStringExtra(TAG_ID));
                    new_intent.putExtra(TAG_TYPE, intent.getStringExtra(TAG_TYPE));
                    new_intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    context.startActivity(new_intent);
                } else if (context != null) {
                    Log.e("Meteor created", place + "/ Not logged in");
                    Intent startActivity = new Intent(context, LoginActivity.class);
                    startActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    context.startActivity(startActivity);
                }
                //place = -1;
                break;
            default:
                Log.e("Meteor created", "Recreated");
                break;

        }
    }

    @Override
    public void onDisconnect() {
        Log.e("Meteor", "disconnected");
    }

    @Override
    public void onException(Exception e) {
        Log.e(ConstantsHolder.TAG_METEOR_EXCEPTION, e.getMessage());
//        if (FunctionsHolder.isOnline(context)) {
//            Intent intent = new Intent(context, SplashActivity.class);
//            context.startActivity(intent);
//        }
    }


    @Override
    public void onDataAdded(String collectionName, String documentID, String newValuesJson) {
        Log.e("METEOR_onDataAdded", "Data added to <" + collectionName + "> in document <" + documentID + ">" + " json " + newValuesJson);
        if (collectionName.equals(ConstantsHolder.COLLECTION_APPLICATIONS)) {
            RelogBus.getInstance().post(new ActiveOrderFragment.OrdersLoadedEvent());
            RelogBus.getInstance().post(new FinishedOrderFragment.OrdersLoadedEvent());
        } else if (collectionName.equals(ConstantsHolder.COLLECTION_COMMENTS)) {
            RelogBus.getInstance().post(new ChatFragment.CommentsLoadedEvent());
        }
    }

    @Override
    public void onDataChanged(String collectionName, String documentID, String updatedValuesJson, String removedValuesJson) {
        Log.e("METEOR_onDataChanged", "Data added to <" + collectionName + "> in document <" + documentID + ">" + " json " + updatedValuesJson);
        if (collectionName.equals(ConstantsHolder.COLLECTION_APPLICATIONS)) {
            RelogBus.getInstance().post(new ActiveOrderFragment.OrdersLoadedEvent());
            RelogBus.getInstance().post(new FinishedOrderFragment.OrdersLoadedEvent());
        } else if (collectionName.equals(ConstantsHolder.COLLECTION_COMMENTS)) {
            RelogBus.getInstance().post(new ChatFragment.CommentsLoadedEvent());
        }
    }

    @Override
    public void onDataRemoved(String collectionName, String documentID) {
        Log.e("METEOR_onDataRemoved", "Data added to <" + collectionName + "> in document <" + documentID + ">");
        if (collectionName.equals(ConstantsHolder.COLLECTION_APPLICATIONS)) {
            RelogBus.getInstance().post(new ActiveOrderFragment.OrdersLoadedEvent());
            RelogBus.getInstance().post(new FinishedOrderFragment.OrdersLoadedEvent());
        } else if (collectionName.equals(ConstantsHolder.COLLECTION_COMMENTS)) {
            RelogBus.getInstance().post(new ChatFragment.CommentsLoadedEvent());
        }
    }
}
