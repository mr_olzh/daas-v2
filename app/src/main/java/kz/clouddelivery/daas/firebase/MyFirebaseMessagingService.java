package kz.clouddelivery.daas.firebase;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

import kz.clouddelivery.daas.R;
import kz.clouddelivery.daas.activities.SplashActivity;


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "firebaseMsgService";
    private static final String TAG_TYPE = "type";
    private static final String TAG_DOCUMENT_ID = "documentId";
    private static final String TAG_ID = "id";
    private static final String TAG_TITLE = "title";
    private static final String TAG_BODY = "body";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e(TAG, "From FCM: " + remoteMessage.getFrom() + "/" + remoteMessage);

        Map<String, String> data = remoteMessage.getData();
        if (data.size() > 0) {
            Intent intent = new Intent(this, SplashActivity.class);
            String type = data.get(TAG_TYPE) == null ? "" : data.get(TAG_TYPE);
            String documentId = data.get(TAG_DOCUMENT_ID);
            intent.putExtra(TAG_TYPE, type);
            intent.putExtra(TAG_ID, documentId);
            Log.e("Meteor created ", "FirebaseMessagingService" + type + "/" + documentId);

            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                    PendingIntent.FLAG_ONE_SHOT);
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(data.get(TAG_TITLE))
                    .setContentText(data.get(TAG_BODY))
                    .setSound(defaultSoundUri)
                    .setAutoCancel(true)
                    .setContentIntent(pendingIntent);
            notificationManager.notify(0, mBuilder.build());
        }

        if (remoteMessage.getNotification() != null) {
            Log.e(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

    }
}